#include<iostream>
#include<bits/stdc++.h>

#define MAX 10000

struct Stack{
    int arr[MAX];
    int top;
    Stack(){
        top = -1;
        minelement = INT_MAX;
    }
    int minelement;
    void Insert(int data);
    void pop();
    int topvalue();
    int getMinElement();
};

int Stack::getMinElement()
{
    return minelement;
}

void Stack::Insert(int data){
    
    if(top == -1)
    {
        top = top+1;
        minelement = data;
    }
    else{
        top = top+1;
        if(data<minelement)
        {
            data = 2*data - minelement;
            minelement= data;
        }
    }
    
    
    arr[top] = data;
}
void Stack::pop(){

    if(arr[top] < minelement){
        minelement = 2*minelement - arr[top];
    }
    top = top-1;
}

int Stack::topvalue(){
    if(top == -1){
        return -1;
    }
    else
    {
        return arr[top];
    }
}

using namespace std;
int main(){
    int query;
    cin>>query;
    Stack st = Stack();
    st.Insert(10);
    cout<<st.topvalue()<<endl;
    st.Insert(20);
    cout<<st.getMinElement()<<endl;
    cout<<st.topvalue()<<endl;
    st.pop();
    cout<<st.topvalue()<<endl;
    return 0;
}