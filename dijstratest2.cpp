#include<bits/stdc++.h>

using namespace std;

#define PII pair<int,int>
const int MAX = 1e4+5;
vector<PII> graph[MAX];
vector<int> dist(MAX,INT_MAX);
vector<int> flag(MAX,0);


void dijstra(int source_vertex){
    priority_queue<PII,vector<PII>,greater<PII>> pq;
    pq.push({0,source_vertex});
    dist[source_vertex] = 0;
    while(!pq.empty()){
        PII temp =pq.top();
        pq.pop();
        int node = temp.second;
        flag[node] = 1;
        for(int i=0;i<graph[node].size();i++){
            int temp_node = graph[node][i].second;
            int temp_weight = graph[node][i].first;
            if(flag[temp_node] == 0 && dist[temp_node] > dist[node]+temp_weight)
            {
                dist[temp_node] = dist[node] + temp_weight;
                pq.push(make_pair(dist[temp_node],temp_node));
            }
        }
    }
}