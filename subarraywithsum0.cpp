#include<bits/stdc++.h>


//print the count of subarrays having sum 0 
using namespace std;

int main(){
    int number;
    cin>>number;
    vector<int>arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }
    unordered_map<int,int> mt;
    int  sum = 0;
    int count =0;
    for(int i=0;i<number;i++){
        sum+=arr[i];
        if(sum == 0){
            count++;
        }
        if(mt.find(sum) != mt.end()){
            count += mt[sum];
        }
        mt[sum]++;
    }
    cout<<"Count "<<count<<endl;
    return 0;
}