#include<bits/stdc++.h>

using namespace std;


int main(){
    int number1,number2;
    cin>>number1>>number2;
    int graph[number1][number2];
    memset(graph,0,sizeof(graph));
    for(int i=0;i<number1;i++){
        for(int j=0;j<number2;j++){
            cin>>graph[i][j];
        }
    }
    queue<pair<int,int>> qt;
    for(int i=0;i<number1;i++){
        for(int j=0;j<number2;j++){
            if(graph[i][j] == 2){
                pair<int,int> pt = make_pair(i,j);
                qt.push(pt);
            }
        }
    }
    qt.push(make_pair(-1,-1));
    int time= 0;
    while(!qt.empty()){
        pair<int,int> pt = qt.front();
        qt.pop();
        if(pt.first == -1 && pt.second == -1 && !qt.empty()){
            cout<<"updating"<<qt.size()<<endl;
            time++;
            qt.push(make_pair(-1,-1));
            continue;
        }
        int index1 = pt.first;
        int index2 = pt.second;
        if(index1 -1 >0){
            if(graph[index1-1][index2] == 1){
                graph[index1 - 1][index2] = 2;
                qt.push(make_pair(index1-1,index2));
            }
        }
        if(index1 +1 < number1){
            if(graph[index1+1][index2] == 1){
                graph[index1 + 1][index2] = 2;
                qt.push(make_pair(index1 + 1,index2));
            }
        }
        if(index2 - 1 > 0){
            if(graph[index1][index2-1] == 1){
                graph[index1][index2 -1] = 2;
                qt.push(make_pair(index1,index2-1));
            }
        }
        if(index2 + 1 < number2){
            if(graph[index1][index2+1] == 1){
                graph[index1][index2+1] = 2;
                qt.push(make_pair(index1,index2+1));
            }
        }
    }
    int flag = 0;
    for(int i=0;i<number1;i++){
        for(int j=0;j<number2;j++){
            if(graph[i][j] == 1){
                flag = 1;
                break;
            }
        }
    }
    if(flag == 0)
    cout<<"Time Taken"<<time<<endl;
    else{
        cout<<"Cannot be made rotten"<<endl;
    }
    return 0;
}