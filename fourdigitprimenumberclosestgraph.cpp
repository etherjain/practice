#include<bits/stdc++.h>

using namespace std;

vector<int> graph[10000];

int visited[10000]={0};

int seieve[10000]={0};

vector<int> prime_numbers;

void prime_seieve(int number)
{
    cout<<"Inside Seieve"<<endl;
    for(int i=2;i*i<number;i++)
    {
        if(seieve[i] == 0)
        {
            for(int j=i*i;j<number;j+=i)
            {
                seieve[j] = 1;
            }
        }
    }
    for(int i=2;i<number;i++)
    {
        if(seieve[i]==0)
        prime_numbers.push_back(i);
    }
    cout<<"Prime Number"<<prime_numbers.size()<<endl;
}

bool compare(int number1,int number2)
{
    string s1 = to_string(number1);
    string s2 = to_string(number2);
    if(s1.length() == s2.length())
    {
        int count =0;
        for(int i=0;i<s1.length();i++)
        {
            if(s1[i] != s2[i])
            {
                count++;
            }
        }
        return count==1?true:false;
    }
    else
    {
        return false;
    }
}

int bfs(int number1,int number2)
{
    queue<int> q;
    q.push(number1);
    visited[number1] = 1;
    while(!q.empty())
    {
        int temp = q.front();
        q.pop();
        for(int i=0;i<graph[temp].size();i++)
        {  
            if(!visited[graph[temp][i]])
            {
                visited[graph[temp][i]] = visited[temp]+1;
                q.push(graph[temp][i]);
            }
            if(graph[temp][i] == number2)
            {
                return visited[graph[temp][i]]-1;
            }
        }
    }
}

int main()
{
    int number1,number2;
    cin>>number1>>number2;
    int number = 10000;
    prime_seieve(number);

    // for(int i=0;i<prime_numbers.size();i++)
    // {
    //     cout<<prime_numbers[i]<<" ";
    // }
    // cout<<endl;
    for(int i=0;i<prime_numbers.size();i++)
    {
        for(int j = i+1;j<prime_numbers.size();j++)
        {
            if(compare(prime_numbers[i],prime_numbers[j]))
            {
                graph[prime_numbers[i]].push_back(prime_numbers[j]);
                graph[prime_numbers[j]].push_back(prime_numbers[i]);
            }
        }
    }
    
    cout<<bfs(number1,number2)<<endl;
    return 0;
}