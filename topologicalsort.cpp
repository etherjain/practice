#include<bits/stdc++.h>

using namespace std;

int graph[1000][1000] = {0};
int visited[1000] = {0};

void topologicalsort(int start_vertex,int vertices,stack<int> &st){
    visited[start_vertex] = true;
    for(int i=1;i<vertices;i++){
        if(graph[start_vertex][i]== 1){
            if(visited[i] == 0){
                topologicalsort(i,vertices,st);
            }
        }
    }
    st.push(start_vertex);
}

void topomain(int vertices){
    stack<int> st;
    for(int i=1;i<=vertices;i++){
        if(visited[i] == 0)
        topologicalsort(i,vertices,st);
    }
    while(!st.empty()){
        cout<<st.top()<<" ";
        st.pop();
    }
}

int main(){
    int vertices,edges;
    cin>>vertices>>edges;
    for(int i=0;i<edges;i++){
        int temp1,temp2;
        cin>>temp1>>temp2;
        graph[temp1][temp2] = 1;
    }
    topomain(vertices);
    return 0;
}