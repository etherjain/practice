number_dict = {1:"I",2:"II",3:"III",4:"IV",5:"V",6:"VI",7:"VII",8:"VIII",9:"IX",10:"X",40:"XL",50:"L",90:"XC",100:"C",400:"CD",500:"D",900:"CM",1000:"M"}
def get_basevalue(number):
    k = list(number_dict.keys())
    for i in range(len(k)):
        if k[i] > number:
            return k[i-1]

def get_roman_number(number):
    if number == 0:
        return ""
    base = get_basevalue(number)
    r_ = number // base
    temp = number_dict[base]*r_
    left = number % base
    return temp + get_roman_number(left)



number = int(input())
print(get_roman_number(number))
