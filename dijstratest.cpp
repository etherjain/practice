#include<bits/stdc++.h>
using namespace std;

#define MAX 10000
vector< pair<int,int>> graph[MAX];
int visited[MAX];
int dis[MAX];


void djistra(int source_vertex,int vertices){
    multiset<pair<int,int>> mt;
    memset(visited,0,sizeof(visited));
    memset(dis,INT_MAX,sizeof(dis));
    mt.insert(make_pair(0,source_vertex));
    while(!mt.empty()){
        pair<int,int> pt = *mt.begin();
        mt.erase(*mt.begin());
        int weight = pt.first;
        int node = pt.second;
        if(visited[node] == 1){
            continue;
        }
        visited[node] = 1;
        for(int i=0;i<graph[node].size();i++){
            pair<int,int> pt2 = graph[node][i];
            int node2 = pt2.first;
            int weight2 = pt2.second;
            if(dis[node] + weight2 < dis[node2]){
                dis[node2]= dis[node] + weight2;
                mt.insert(make_pair(dis[node2],node2));
            }
            
        }
    }
}

