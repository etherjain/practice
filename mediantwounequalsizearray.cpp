#include<bits/stdc++.h>

using namespace std;

vector<int> mergeTwoArray(vector<int> a,vector<int> b)
{
    int length1 = a.size();
    int length2 = b.size();
    vector<int> result ;
    int i=0,j=0;
    while(i<length1 && j<length2)
    {
        if(a[i]<b[j])
        {
            result.push_back(a[i]);
            i++;
        }
        else if(b[j]<a[i])
        {
            result.push_back(b[j]);
            j++;
        }
        else{
            i++;
            j++;
        }
    }

    while(i<length1)
    {
        result.push_back(a[i]);
        i++;
    }
    while(j<length2)
    {
        result.push_back(b[j]);
        j++;
    }

    return result;
}

int main(){

    vector<int> number1;
    vector<int> number2;

    int length1,length2;
    cin>>length1>>length2;
    for(int i=0;i<length1;i++)
    {
        int temp;
        cin>>temp;
        number1.push_back(temp);
    }
    for(int i=0;i<length2;i++)
    {
        int temp;
        cin>>temp;
        number2.push_back(temp);
    }

    vector<int> merged = mergeTwoArray(number1,number2);

    int merged_size = merged.size();
    if(merged_size%2==0)
    {
        cout<<(merged[(merged_size/2)-1]+merged[merged_size/2])/2<<endl;

    }
    else
    {
        cout<<merged[merged_size/2]<<endl;
    }
    return 0;
}