#include<bits/stdc++.h>

using namespace std;


int extendedEculidian(int a,int b,int &x,int &y){
    if(a==0){
        x=0;
        y=1;
        return b;
    }

    int x1,y1;
    int gcd = extendedEculidian(b%a,a,x1,y1);

    x = y1 - (b/a) * x1;
    y = x1;
    
    return gcd;
}

int main(){

    int number1,number2;
    cin>>number1>>number2;
    int x,y;
    cout<<extendedEculidian(number1,number2,x,y)<<endl;
    cout<<"x "<<x<<" y "<<y<<endl;
    return 0;

}