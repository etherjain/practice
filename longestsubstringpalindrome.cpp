#include<bits/stdc++.h>

using namespace std;

int longestpalindromedynamic(string s, int length)
{
    int arr[length][length];
    memset(arr,0,sizeof(arr));
    for(int i=0;i<length;i++)
    {
        arr[i][i]=1;
    }

    int start = 1;
    int max_len = 1;

    for(int i=0;i<length-1;i++)
    {
        if(s[i] == s[i+1])
        {
            arr[i][i+1]=1;
            start = i;
            max_len = 2;
        }
    }
    for(int i=3;i<length;i++){
        for(int j=0;j<length-i+1;j++){
            
            //last index of the subarray
            int k = j+i-1;
            if(arr[j+1][k-1] && s[j] == s[k])
            {
                arr[j][k] = 1;
                if(i>max_len){
                    max_len = i;
                    start = j;
                }
            }
        }
    }
    for(int i=start;i<start+max_len;i++){
        cout<<s[i];
    }
    cout<<endl;
    return max_len;
}

int main(){
    string s;
    cin>>s;
    int length = s.length();
    cout<<longestpalindromedynamic(s,length)<<endl;
    return 0;

}