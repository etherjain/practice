//merge two sorted linkedlist inplace O(1)space 

#include<bits/stdc++.h>

using namespace std;



struct Node{
    int data;
    Node* next = NULL;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
}


Node* mergeLinkedListRecursive(Node* a,Node* b){
    Node* result = NULL;
    if(a == NULL){
        return b;
    }
    if(b == NULL){
        return a;
    }
    if(a == NULL && b== NULL){
        return NULL;
    }
    if(a->data > b->data){
        result = b;
        result->next = mergeLinkedListRecursive(b0b)
    }
}
Node* mergeLinkedList(Node* a,Node* b){
    if(a == NULL && b!=NULL){
        return b;
    }
    if(b == NULL && a!=NULL){
        return a;
    }
    if(a == NULL && b == NULL){
        return NULL;
    }
    Node* head  = NULL;
    if(a->data < b->data){
        head = a;
    }
    else
    {
        head = b;
    }
    Node* prev_ptr1;
    Node* prev_ptr2;
    while(a!= NULL && b!=NULL){
        if(a->data < b->data){
            prev_ptr1 = a;
            a = a->next;
        }
        else
        {
            Node* temp_ptr1 = prev_ptr1->next;
            Node* temp_ptr2 = b->next;
            prev_ptr1->next = b;
            b->next = temp_ptr1;
            a = b;
            b = temp_ptr2;
        }
    }
    if(b!=NULL){
        prev_ptr1->next = b;
    }
    return head;
}
void printNode(Node* head){
    Node* temp =head;
    while(temp!=NULL){
        cout<<temp->data<<" ";
        temp =temp->next;
    }
    cout<<endl;   
}
int main(){
    Node* head1 = addNode(1);
    head1->next = addNode(2);
    head1->next->next = addNode(6);
    Node* head2 = addNode(3);
    head2->next = addNode(5);
    head2->next->next = addNode(7);
    head2->next->next->next = addNode(8);
    Node* new_head = mergeLinkedList(head1,head2);
    printNode(new_head);
}