#include<bits/stdc++.h>
#include<limits.h>
using namespace std;

#define INF INT_MAX



int main(){
    int vertices,edges;
    cin>>vertices>>edges;
    int graph[vertices][vertices];
    // memset(graph,INF,sizeof(graph));
    // cout<<INF<<endl;
    for(int i=0;i<vertices;i++)
    {
        for(int j=0;j<vertices;j++)
            graph[i][j] = i == j?0:INF; 
    }

    for(int i=0;i<edges;i++)
    {
        int temp1,temp2,value;
        cin>>temp1>>temp2>>value;
        graph[temp1][temp2] = value;
    }

    //  for(int i=0;i<vertices;i++)
    // {
    //     for(int j=0;j<vertices;j++)
    //     {
    //         cout<<graph[i][j]<<" ";
    //     }
    //     cout<<endl;
    // }

    int path[vertices][vertices];
    for(int i=0;i<vertices;i++)
        for(int j=0;j<vertices;j++)
            path[i][j] = i == j?0:i;

    for(int k=0;k<vertices;k++)
    {
        for(int i=0;i<vertices;i++)
        {
            for(int j=0;j<vertices;j++)
            {
                if(graph[i][k]!=INF && graph[k][j]!=INF && graph[i][j] > graph[i][k]+graph[k][j])
                {
                    graph[i][j] =  graph[i][k]+graph[k][j];
                    path[i][j] = k+1;
                }
            }
        }
    }

    for(int i=0;i<vertices;i++)
    {
        for(int j=0;j<vertices;j++)
        {
            cout<<graph[i][j]<<" ";
        }
        cout<<endl;
    }

    cout<<"path matrix"<<endl;
    for(int i=0;i<vertices;i++)
    {
        for(int j=0;j<vertices;j++)
        {
            cout<<path[i][j]<<" ";
        }
        cout<<endl;
    }
    return 0;

}