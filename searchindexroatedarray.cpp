#include<bits/stdc++.h>

using namespace std;

int binarySearchRotated(vector<int>arr,int l,int r,int key)
{
    if(l<=r)
    { 
        int mid=(l+r)/2;
        if(arr[mid]==key)
        {
            return mid;
        }
        if(arr[l]<=arr[mid])
        {
            if(arr[l]<=key && arr[mid]>=key)
            {
                return binarySearchRotated(arr,l,mid-1,key);
            }
            else{
                return binarySearchRotated(arr,mid+1,r,key);
            }

        }
        else if(arr[mid]<=arr[r])
        {
            if(arr[mid]<=key && arr[r]>=key)
            {
                return binarySearchRotated(arr,mid+1,r,key);
            }
            else
            {
                return binarySearchRotated(arr,l,mid-1,key);
            }
        }
    }
    return -1;
}
int main()
{
    int test;
    cin>>test;
    while(test--)
    {
        int number;
        cin>>number;
        vector<int> arr;
        for(int i=0;i<number;i++)
        {
            int temp;
            cin>>temp;
            arr.push_back(temp);
        }
        int value;
        cin>>value;
        cout<<"value"<<binarySearchRotated(arr,0,arr.size()-1,value)<<endl;
    }
    return 0;
}