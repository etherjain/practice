#include<bits/stdc++.h>


using namespace std;

void dfs(vector<int> graph[],int vertices,int visited[],int root)
{
    stack<int> s;
    s.push(root);
    while(!s.empty())
    {
        int temp = s.top();
        visited[temp]=1;
        cout<<temp<<" ";
        s.pop();
        for(int i=0;i<graph[temp].size();i++)
        {
            if(visited[graph[temp][i]]!=1)
            {
                s.push(graph[temp][i]);
            }
        }
    }
}

int main(){
    int test;
    cin>>test;
    while(test--)
    {
        int vertices;
        cin>>vertices;
        int edges;
        cin>>edges;
        vector<int> graph[vertices];
        int visited[vertices]={0};
        for(int i=0;i<edges;i++)
        {
            int temp1,temp2;
            cin>>temp1>>temp2;
            graph[temp1].push_back(temp2);
            // graph[temp2].push_back(temp1);
        }
        dfs(graph,vertices,visited,2);
    }
}