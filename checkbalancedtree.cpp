#include<bits/stdc++.h>
using namespace std;

//check if the height differenve between the left and the right subtree is not more than 1
struct Node{
    int data;
    Node* left;
    Node* right;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->right = NULL;
    temp->left = NULL;
    return temp;
}

int check_balanced(Node* root){
    if(root == NULL){
        return 0;
    }
    if(root->left == NULL && root->right == NULL){
        return 1;
    }
    int lefth = check_balanced(root->left);
    int righth = check_balanced(root->right);

    if(abs(lefth-righth) > 1){
        return -1;
    }
    else
    {
        return max(lefth,righth) + 1;
    }
}

int main(){
    Node *root = addNode(-15); 
    root->left = addNode(5); 
    root->right = addNode(6); 
    root->left->left = addNode(-8); 
    root->left->left->left = addNode(90);
    // root->left->right = addNode(1); 
    if(check_balanced(root) != -1){
        cout<<"true"<<endl;
    }
    else
    {
        cout<<"false"<<endl;
    }
    return 0;
}