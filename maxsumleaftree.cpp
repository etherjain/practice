#include<bits/stdc++.h>

//find maximum sum  between two leaves in a tree.it can ingore the root node also .
using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->right = NULL;
    temp->left = NULL;
    return temp;
}

int maxSumUtil(Node* root,int &maxsubtreesum){
    if(root == NULL){
        return 0;
    }
    if (root->left == NULL && root->right == NULL){
        return root->data;
    }

    int ls = maxSumUtil(root->left,maxsubtreesum);
    int rs = maxSumUtil(root->right,maxsubtreesum);

    if(root->left!= NULL && root->right!= NULL){
        maxsubtreesum = max(maxsubtreesum,ls+rs+root->data);
        return max(ls,rs) + root->data;
    }
    else
    {
        return root->left!=NULL?ls+root->data:rs+root->data;
    }
}

int main(){
    Node *root = addNode(-15); 
    root->left = addNode(5); 
    root->right = addNode(6); 
    root->left->left = addNode(-8); 
    root->left->right = addNode(1); 
    root->left->left->left = addNode(2); 
    root->left->left->right = addNode(6); 
    root->right->left = addNode(3); 
    root->right->right = addNode(9); 
    root->right->right->right= addNode(0); 
    root->right->right->right->left= addNode(4); 
    root->right->right->right->right= addNode(-1); 
    root->right->right->right->right->left= addNode(10);
    int maxsumtillnow = INT_MIN ;
    maxSumUtil(root,maxsumtillnow);
    cout<<"Max Value "<<maxsumtillnow<<endl;
}