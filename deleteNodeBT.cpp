#include<bits/stdc++.h>

struct Node{
    int data;
    struct Node* left=NULL;
    struct Node* right=NULL;
};
Node* deleteDeepNode(Node* root,Node* keynode)
{  
    std::cout<<"Deleting Deep Node"<<std::endl;
    std::queue<Node*> q;
    Node* temp_root = root;
    if(root==NULL)
    {
        return root;
    }
    q.push(root);
    while(!q.empty())
    {
        Node* temp = q.front();
        q.pop();
        if(temp->left)
        {
            if(temp->left==keynode)
            {
                temp->left=NULL;
                delete(keynode);
                break;
            }
            else
            {
                q.push(temp->left);
            }
        }
        if(temp->right)
        {
            if(temp->right==keynode)
            {
                temp->right=NULL;
                delete(keynode);
                break;
            }
            else{
                q.push(temp->right);
            }
        }
    }
    return temp_root;
}

void printInorder(Node* root)
{
    if(!root)
    {
        return;
    }

    printInorder(root->left);
    std::cout<<root->data<<" ";
    printInorder(root->right);
}
Node* deleteNode(Node* root,int key)
{
    Node* temp_root = root;
    std::queue<Node*> q;
    q.push(root);
    Node *key_node = NULL;
    Node* temp;
    while(!q.empty())
    {
        temp = q.front();
        q.pop();
        if(temp->data==key)
        {
            key_node = temp;
        }
        if(temp->left)
        {
            q.push(temp->left);
        }
        if(temp->right)
        {
            q.push(temp->right);
        }
    }
    std::cout<<"Found Node"<<std::endl;
    int data = temp->data;
    temp_root=deleteDeepNode(temp_root,temp);
    key_node->data=data;
    printInorder(temp_root);
    return temp_root;
}

Node* newNode(int key)
{
    Node* temp= new Node;
    temp->data=key;
    return temp;
}
int main(){
    struct Node* root = newNode(10); 
    root->left = newNode(11); 
    root->left->left = newNode(7); 
    root->left->right = newNode(12); 
    root->right = newNode(9); 
    root->right->left = newNode(15); 
    root->right->right = newNode(8); 
  
    std::cout << "Inorder traversal before deletion : "; 
    printInorder(root); 
  
    int key = 12; 
    root=deleteNode(root, key); 
  
    std::cout << std::endl; 
    std::cout << "Inorder traversal after deletion : "; 
    printInorder(root); 
  
    return 0; 
}