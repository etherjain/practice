#include<bits/stdc++.h>

using namespace std;

int searchRotatedArrayUpdate(vector<int> arr,int l,int r)
{
    if(l<=r)
    {
        if(l==r)
        {
            return arr[r];
        }
        
        if(r==l+1)
        {
            if(arr[l]>=arr[r])
            {
                return arr[l];
            }
            else
            {
                return arr[r];
            }
        }

        int mid=(l+r)/2;
        if(arr[mid]>arr[mid+1]&&arr[mid]>arr[mid-1])
        {
            return arr[mid];
        }
        else
        {
            if(arr[mid]>arr[mid+1] &&arr[mid]<arr[mid-1])
            {
                return searchRotatedArrayUpdate(arr,l,mid-1);
            }
            else
            {
                return searchRotatedArrayUpdate(arr,mid+1,r);
            }
        }
    }
    
    return -1;

}