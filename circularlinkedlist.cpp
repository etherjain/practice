#include<bits/stdc++.h>

struct Node{
    int data;
    struct Node* next=NULL;
};

int countNode(Node *N);
void detectCircle(Node* root)
{
    Node* fwd=root;
    Node* bck =root;

    while(fwd && bck && fwd->next)
    {
        fwd = fwd->next->next;
        bck = bck->next;
        if(fwd == bck)
        {
            std::cout<<"circular"<<std::endl;
            std::cout<<"count"<<countNode(fwd)<<std::endl;
            return;
        }
    }
    std::cout<<"No Cycle"<<std::endl;
    return;
}

struct MiddleNode{
    struct Node* N = NULL;
    int midIndex;
};

Node* getMiddleElement(Node* N)
{
    Node* first = N;
    Node* second = N;
    
    if(N!=NULL && N->next!=NULL)
    {
        while(second!=NULL && second->next!=NULL)
        {
            first=first->next;
            second = second->next->next;
        }
    }
    return first;
}
int countNode(Node* head)
{
    int count =0;
    Node* temp = head;
    while(temp->next != head)
    {
        count++;
        temp = temp->next;
    }
    return count+1;
}
Node* push(Node* head,int key)
{
    Node* temp= new Node;
    temp->data = key;
    if(head==NULL)
    {
        return temp;
    }
    else
    {
        head->next = temp;
    }
    head=temp;
    return head;
}
int main(){
    int number;
    std::cin>>number;
    Node* head = NULL;
    Node* temp= push(head,10);
    if(head==NULL){
        head = temp;
    }
    temp = push(temp,20);
    temp = push(temp,30);
    temp = push(temp,40);
    // temp = push(temp,50);
    // temp->next = head->next;
    // Node* temp2 =  temp;
    std::cout<<"Print"<<std::endl;
    // while(temp2)
    // {
    // std::cout<<temp2->data<<std::endl;
    // temp2 = temp2->next;
    // }
    detectCircle(head);
    std::cout<<getMiddleElement(head)->data<<std::endl;
    return 0;
}