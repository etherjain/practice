#include<bits/stdc++.h>

using namespace std;

#define MAX_ 10000

int arr[MAX_][MAX_];
int color[MAX_];

bool bipartite_(int startingvertex,int vertices)
{
    bool flag = true;
    queue<int> q;
    q.push(startingvertex);
    while(!q.empty())
    {
        int temp = q.front();
        q.pop();
        if(color[temp] == -1){
            color[temp] = 1;
        }
        for(int i=1;i<=vertices;i++){
            if(arr[temp][i]==1){
                if(color[i] == -1){
                    if(color[temp] == 1) color[i] = 2;
                    if(color[temp] == 2) color[i] = 1;
                    q.push(i);
                }
                else if(color[i] == color[temp]){
                    return false;
                }
                else if(color[i] != color[temp]){
                    continue;
                }
            }
        }
    }
    for(int i=1;i<=vertices;i++)
    {
        cout<<color[i]<<" ";
        
    }
    cout<<endl;
    return flag;
}

bool bipartite(int vertices){
    bool flag = true;
    for(int i=1;i<=vertices;i++){
        if(color[i] == -1){

            if(!bipartite_(i,vertices)) return false;
        }
    }
    return flag;
}


int main(){
    memset(arr,0,sizeof(arr));
    memset(color,-1,sizeof(color));
    int vertices,edges;
    cin>>vertices>>edges;
    for(int i=0;i<edges;i++){
        int temp1,temp2;
        cin>>temp1>>temp2;
        arr[temp1][temp2]=1;
        arr[temp2][temp1] = 1;
    }

    cout<<bipartite(vertices)<<endl;

}