#include<bits/stdc++.h>

using namespace std;

int main(){
    int number;
    cin>>number;
    vector<int> arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }
    int curr_max=arr[0];
    int max_value = arr[0];
    for(int i=1;i<number;i++){
    curr_max = max(arr[i],arr[i]+curr_max);
    max_value = max(max_value,curr_max);
    }
    cout<<max_value<<endl;
}