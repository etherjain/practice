#include<bits/stdc++.h>

using namespace std;

/**
 * Addition without using airthemetic operation
 */
int main(){
    int number1;
    int number2;
    cin>>number1>>number2;
    int result = 0;
    while(number2)
    {
        int carry = number1 & number2;
        result  = number1 ^ number2;
        number1 = result;
        number2 = carry << 1;
    }
    cout<<result;
    return 0;
}