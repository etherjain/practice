#include<bits/stdc++.h>

using namespace std;



int main(){
    int number;
    cin>>number;
    vector<int> arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }
    
    int sum ;
    cin>>sum;
    int curr_sum = 0;
    int start_index =0;
    int end_index = 0;
    int flag = 0;
    for(int i=0;i<number;i++){
        curr_sum += arr[i];
        if(curr_sum == sum){
            end_index = i;
            flag =1 ;
            break;
        }
        while(curr_sum > sum){
            curr_sum -=arr[start_index];
            start_index++;
            if(curr_sum == sum){
                end_index = i;
                flag = 1;
                break;
            }
        }
    }
    if(flag == 1){
        cout<<start_index<<" "<<end_index<<endl;
    }
    return 0;
}