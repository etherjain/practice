#include<iostream>
using namespace std;

#define MAX 1000

struct Queue{
    int arr[MAX];
    int front,rear;
    int capacity;
    int top();
    void push(int data);
    void pop();
    Queue(){
        front = -1;
        rear = -1;
        capacity = 0;
    }
};

void Queue::push(int data)
{
    if(capacity == MAX){
        cout<<"queuefull"<<endl;
        return;
    }
    capacity+=1;
    rear = (rear+1)%MAX;
    if(front == -1)
    {
        front = front+1;
    }
    arr[rear] = data;
}
int Queue::top(){
    if(capacity ==0){
        cout<<"empty"<<endl;
        return -1;
    }
    return arr[front];
}
void Queue::pop(){
    if(capacity == 0){
        cout<<"Nothing to pop"<<endl;
        return;
    }
    arr[front]=0;
    front = (front+1)%MAX;
    capacity--;
}
int main(){
    Queue qt = Queue();
    qt.push(10);
    qt.push(100);
    cout<<qt.top()<<endl;
    qt.pop();
    qt.push(200);
    cout<<qt.top()<<endl;
    qt.pop();
    cout<<qt.top()<<endl;
    qt.pop();
    qt.pop();
    qt.top();
    return 0;
}