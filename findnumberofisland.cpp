#include<bits/stdc++.h>

using namespace std;

struct Ranking{
    int parent;
    int rank;
};

int find(Ranking ranks[],int index)
{
    /*

    Find using path compression
    */

    if(ranks[index].parent == index)
    {
        return index;
    }
    return find(ranks,ranks[index].parent);
    // return ranks[index].parent; 
}

void Union(Ranking ranks[],int index1,int index2)
{
    /*
    Union using union of rank
    */
   int temp_root1 = find(ranks,index1);
   int temp_root2 = find(ranks,index2);

   if(ranks[temp_root1].rank > ranks[temp_root2].rank)
   {
       ranks[temp_root2].parent = temp_root1; 
   }
   else if(ranks[temp_root1].rank < ranks[temp_root2].rank)
   {
       ranks[temp_root1].parent = temp_root2;
   }
   else
   {
       ranks[temp_root2].parent = temp_root1;
       ranks[temp_root1].rank++;
   }
}



int main(){
    int rows,coloms;
    cin>>rows>>coloms;
    int arr[5][5] = {   {1, 1, 0, 0, 0}, 
                                {0, 1, 0, 0, 1}, 
                                {1, 0, 0, 1, 1}, 
                                {0, 0, 0, 0, 0}, 
                                {1, 1, 1, 0, 1} 
                            };
    // for(int i=0;i<rows;i++)
    // {
    //     for(int j=0;j<coloms;j++)
    //     {
    //         cin>>arr[i][j];
    //     }
    // }

    Ranking ranks[rows*coloms];
    for(int i=0;i<rows*coloms;i++)
    {
        ranks[i].parent = i;
        ranks[i].rank = 0;
    }
    for(int i=0;i<rows;i++)
    {
        for(int j=0;j<coloms;j++){
            if(arr[i][j] == 1)
            {
                if(i!=0 && arr[i-1][j] == 1)
                {
                    Union(ranks,rows*(i)+j,rows*(i-1)+j);
                }
                if(i!=rows-1 && arr[i+1][j] == 1)
                {
                    Union(ranks,rows*(i)+j,rows*(i+1)+j);
                }
                if(j!=0 && arr[i][j-1] == 1)
                {
                     Union(ranks,rows*(i)+j,rows*(i)+j-1);
                }
                if(j!=coloms-1 && arr[i][j+1] == 1)
                {
                     Union(ranks,rows*(i)+j,rows*(i)+j+1);
                }
                if(i!=rows-1 && j!=coloms-1 && arr[i+1][j+1]==1)
                {
                    Union(ranks,rows*(i)+j,rows*(i+1)+j+1);
                }
                if(i!=0 && j!=0 & arr[i-1][j-1] ==1)
                {
                    Union(ranks,rows*(i)+j,rows*(i-1)+j-1);
                }
                if(i!=rows-1 && j!=0 && arr[i+1][j-1] == 1)
                {
                    Union(ranks,rows*(i)+j,rows*(i+1)+j-1);
                }
                if(i!=0 && j!=coloms-1 && arr[i-1][j+1] ==1)
                {
                    Union(ranks,rows*(i)+j,rows*(i-1)+j+1);
                }
            }
        }
    }
    int count[rows*coloms];
    memset(count,0,sizeof(count));
    int answer = 0;
    for(int i=0;i<rows;i++)
    {
        for(int j=0;j<coloms;j++)
        {
            if(arr[i][j]==1)
            {
                int x = find(ranks,rows*(i)+j);
                cout<<x<<" ";
                if(count[x] ==0 )
                {
                    answer++;
                    count[x]++;
                }
                else
                {
                    count[x]++;
                }
            }
            else
            {
                cout<<"Nan ";
            }            
        }
        cout<<endl;
    }

    cout<<answer<<endl;
    return 0;

}