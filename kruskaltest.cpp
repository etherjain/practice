#include<bits/stdc++.h>

using namespace std;

#define PII pair<long long ,pair<int,int>>
#define PI pair<long long,int>
const int MAX = 1e4+5;

int parent[MAX];

int visited[MAX];
int root(int x){
    
    while(parent[x]!=x){
        parent[x] = parent[parent[x]];
        x = parent[x];
    }
    return x;
}

void Union(int x, int y ){

    int root1 = root(x);
    int root2 = root(y);
    parent[x] = parent[y];
}

long long int kruskal(vector<PII> graph){

    if(graph.size() == 0 ){
        return 0;
    }
    long long int weight = 0;
    sort(graph.begin(),graph.end());
    int i =0;
    while(i<graph.size()){
        PII temp = graph[i];
        long long temp_weight = temp.first;
        int node1 = temp.second.first;
        int node2 = temp.second.second;
        if(root(node1) != root(node2)){
            weight += temp_weight;
            Union(node1,node2);
        }
        i++;
    }

    return weight;

}

int prims(vector<PII> graph,int start_vertex){
    if(graph.size()==0){
        return 0;
    }
    priority_queue<PI,vector<PI>,greater<PI>> pq;

    pq.push({0,start_vertex});
    while(!pq.empty()){
        PI temp = pq.top();
        long long temp_weight = temp.first;
        int temp_node = temp.second;
        visited[temp_node] = 1;
        for(int i=0;i<graph[temp_node].size();i++){
            
        }
    }



}