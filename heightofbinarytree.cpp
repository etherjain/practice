#include<bits/stdc++.h>

using namespace std;


struct Node{
    int data;
    Node* right;
    Node* left;
};

Node* addNode(int data){
    Node* tempNode=  new Node;
    tempNode->data = data;
    tempNode->right = NULL;
    tempNode->left = NULL;
}


Node* inserNode(Node* root,int data){
    if(root == NULL){
    root = addNode(data);
    return root;
    }
    if(data > root->data){
        root->right = inserNode(root->right,data);
    }
    else
    {
        root->left = inserNode(root->left,data);
    }
    return root;
       
}
int getHeight(Node* root)
{
    if(root == NULL){
        return 0;
    }

    return 1+max(getHeight(root->left),getHeight(root->right));

}

int main(){
    int number;
    cin>>number;
    Node* root = NULL;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        root = inserNode(root,temp);
    }
    cout<<"Height Of the Tree "<<getHeight(root)<<endl;
    return 0;
}