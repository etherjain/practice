#include<bits/stdc++.h>

using namespace std;

#define MAX 1000

struct heap{
    int arr[MAX];
    int heapsize;
    heap(){
        heapsize = -1;
    }
    void insert(int data);
    int mindata(){
        return arr[0];
    }
    void delelement(int index);
    void minheapify(int index1);
    void updatekey(int index,int value);
    int extractmin();
};

void heap::insert(int data){
    if(heapsize == -1){
        heapsize = heapsize+1;
        arr[heapsize] = data;
        return;
    }
    else
    {
        if(heapsize == MAX-1){
            cout<<"Full"<<endl;
            return;
        }
        else
        {
            heapsize = heapsize +1;
            arr[heapsize] = data;
            int i = heapsize;
            while(i!=0 && arr[i] < arr[(i-1)/2])
            {
                swap(arr[i],arr[(i-1)/2]);
                i = (i-1)/2;
            }
        }
    }
    
}

void heap::minheapify(int index){
    int left   = 2*index+1;
    int right = 2*index+2;
    int smallest  = index;
    if(left<=heapsize && arr[index] > arr[left]){
        smallest = left;
        swap(arr[left],arr[index]);
    }
    if(right<=heapsize && arr[index] > arr[right]){
        smallest = right;
        swap(arr[right],arr[index]);
    }
    if(smallest != index){
        minheapify(smallest);
    }
}

int heap::extractmin(){
    if(heapsize >= 0){
        int result = arr[0];
        arr[0] = arr[heapsize];
        heapsize--;
        minheapify(0);
    }
}

void heap::delelement(int index){
    if(index > heapsize || index < 0 ){
       return;
    }
    else
    {
        arr[index] = INT_MIN;
        while(index!=0 && arr[index]<arr[(index-1)/2])
        {
            swap(arr[index],arr[(index-1)/2]);
            index = (index-1)/2;
            
        }
    }
    extractmin();
}
void heap::updatekey(int index,int value){
    if(index<0 || index > heapsize)
    {
        cout<<"index out of range"<<endl;
    }
    arr[index] = value;
    while(index!=0 && arr[index]<arr[(index-1)/2]){
        swap(arr[index],arr[(index-1)/2]);
        index = (index-1)/2;
    }
    if(index !=0){
        minheapify(index);
    }
}

int main(){
    heap h = heap(); 
    h.insert(3); 
    h.insert(2); 
    h.delelement(1); 
    h.insert(15); 
    h.insert(5); 
    h.insert(4); 
    h.insert(45); 
    cout << h.extractmin() << " "; 
    cout << h.mindata() << " "; 
    h.updatekey(2, 1); 
    cout << h.mindata();
return 0;
}