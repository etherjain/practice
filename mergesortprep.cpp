#include<bits/stdc++.h>
using namespace std;

void merge(vector<int> &arr,int mid,int l,int r)
{
    cout<<mid<<l<<r<<endl;
    int k = mid-l+1;
    int m = r-mid;

    vector<int> L;
    vector<int> H;
    for(int i=0;i<k;i++){
        L.push_back(arr[l+i]);
    }
    for(int i=0;i<m;i++){
        H.push_back(arr[mid+1+i]);
    }
    int i=0,j=0;
    int p = l;
    while(i<k && j<m){
        if(L[i]<H[j]){
            arr[p] = L[i];
            i++;
        }
        else if(H[j]<L[i]){
            arr[p] = H[j];
            j++;
        }
        else{
            arr[p]= H[j];
            i++;
            j++;
        }
        p++;
    }
    while(i<k){
        arr[p] = L[i];
        i++;
        p++;
    }
    while(j<m){
        arr[p] = H[j];
        j++;
        p++;
    }
    for(int i=l;i<=r;i++){
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}

void mergesort(vector<int> &arr,int l,int h){

    if(l>=h){
        return;
    }
    int mid = (l+h)/2;

    mergesort(arr,l,mid);
    mergesort(arr,mid+1,h);

    merge(arr,mid,l,h);
}

int main(){
    vector<int> test;
    int number;
    cin>>number;
    while(number--){
        int temp;
        cin>>temp;
        test.push_back(temp);
    }
    mergesort(test,0,test.size()-1);

    for(int i=0;i<test.size();i++){
        cout<<test[i]<<" ";
    }
}