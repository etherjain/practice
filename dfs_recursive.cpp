#include<bits/stdc++.h>
using namespace std;
vector<int> graph[10000];
int visited[10000] = {0};
void dfs_util(int);
void dfs(int vertices)
{
    int count =0;
    for(int i=0;i<vertices;i++)
    {
        if(!visited[i])
        {
            count+=1;
            cout<<"test"<<endl;
            dfs_util(i);
        }
    }
    cout<<"Connected Components "<<count<<endl;
}
void dfs_util(int root)
{
    // cout<<"Dfs"<<endl;
    // if(visited[root])
    // {
    //     cout<<"return"<<endl;
    //     return;
    // }
    visited[root]=1;
    cout<<"root "<<root<<endl;

    for(int i=0;i<graph[root].size();i++)
    {
        if(!visited[graph[root][i]])
            dfs_util(graph[root][i]);   
    }
}
int main(){
int test;
cin>>test;
while(test--){
    int vertices;
    cin>>vertices;
    int edges;
    cin>>edges;
    for(int i=0;i<edges;i++)
    {
        int temp1,temp2;
        cin>>temp1>>temp2; 
        graph[temp1].push_back(temp2);
    }
    dfs(vertices);
}
return 0;
}