#include<bits/stdc++.h>

using namespace std;

int interpolationSearch(vector<int>arr,int n,int value)
{
    int lo=0;
    int hi=n-1;
    int pos=0;
    int flag=0;
    while(lo<=hi)
    {
        pos = lo+((value-arr[lo])*(hi-lo))/(arr[hi]-arr[lo]);
        cout<<"pos"<<pos<<endl;
        if(arr[pos]==value)
        {
            flag=1;
            return pos;
        }
        else if(arr[pos]>value)
        {
            hi=pos-1;
        }
        else
        {
            lo=pos+1;
        }
    }
    return -1;
}

int main()
{
    int test;
    cin>>test;
    while(test--)
    {
        int number;
        cin>>number;
        vector<int> arr;
        for(int i=0;i<number;i++)
        {
            int temp;
            cin>>temp;
            arr.push_back(temp);
        }
        int value;
        cin>>value;
        cout<<"value"<<interpolationSearch(arr,arr.size(),value)<<endl;
    }
    return 0;

}