#include<bits/stdc++.h>

using namespace std;

struct Node{
    int data;
    Node* right = NULL;
    Node* left = NULL;
};

Node* newNode(int data)
{
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

Node* makeBinaryTree(Node* root,vector<int> arr,int start,int end){
    if (start > end)
        return NULL;
    int mid = (start+end)/2;
    root = newNode(arr[mid]);
    root->left = makeBinaryTree(root->left,arr,start,mid-1);
    root->right = makeBinaryTree(root->right,arr,mid+1,end);
    return root;
}

int main(){
    vector<int> arr;
    int number;
    cin>>number;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }

    sort(arr.begin(),arr.end());
    Node* root = new Node;
    root = makeBinaryTree(root,arr,0,arr.size()-1);
}