#include<bits/stdc++.h>

struct Node{
    int data;
    struct Node* left=NULL;
    struct Node* right=NULL;
};

void printTree(Node* root)
{
    if(root==NULL)
    {
        return;
    }
    printTree(root->left);
    std::cout<<root->data<<" ";
    printTree(root->right);
}


int main(){
    int test;
    std::cin>>test;
    while(test--)
    {
        int number;
        std::cin>>number;
        Node *root = new Node;
        root->data = 10;
        root->left = new Node;
        root->left->data = 9;
        root->right = new Node;
        root->right->data = 11;
        printTree(root);
    }
    return 0;
}