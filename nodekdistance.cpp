//print nodes at k distance from a node


#include<bits/stdc++.h>

using namespace std;


struct Node{
    int data;
    Node* left=NULL;
    Node* right=NULL;
};



Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left= NULL;
    temp->right = NULL;
    return temp;
}

void travelDown(Node* root,int distance){
    if (root == NULL){
        return;
    }
    if (distance == 0){
        cout<<root->data<<" "<<endl;
        return;
    }

    travelDown(root->left,distance-1);
    travelDown(root->right,distance-1);
}

int findNode(Node* root,Node* target,int distance){
    if(root == NULL || target == NULL){
        return -1;
    }

    if(root == target){
        travelDown(root,distance);
        return 0;
    }
    
    int nodelevel = findNode(root->left,target,distance);

    if(nodelevel != -1){

        if(nodelevel + 1 == distance){
            cout<<root->data<<" ";
        }
        else{
            travelDown(root->right,distance-nodelevel-2);
        }
        return 1 + nodelevel;
    }
    int nodelevel2 = findNode(root->right,target,distance);
    if(nodelevel2 != -1){
        if(nodelevel2 + 1 == distance){
            cout<<root->data<<" ";
        }
        else{
            travelDown(root->left,distance-nodelevel2-2);
        }
        return 1+nodelevel2;
    }

    return -1;
}
