
#include<bits/stdc++.h>

using namespace std;

vector<int> graph[10000];
int visited[10000] = {0};

int find(int parent[],int index)
{
    if(parent[index] == -1)
    {
        return index;
    }
    return find(parent,parent[index]);
}

void Union(int parent[],int index1,int index2)
{
    int temp1 = find(parent,index1);
    int temp2 = find(parent,index2);
    if(temp1 !=  temp2){
        parent[temp1] = temp2;
    }
}


int main(){

    int vertices,edges;
    cin>>vertices>>edges;
    vector< pair<int,int> > edges_list;
    for(int i=0;i<edges;i++)
    {
        int temp1,temp2;
        cin>>temp1>>temp2;
        graph[temp1].push_back(temp2);
        edges_list.push_back(make_pair(temp1,temp2));
    }
    int parent[vertices];
    memset(parent,-1,sizeof(parent));
    for(int i=0;i<edges_list.size();i++)
    {
        auto temp_edge = edges_list[i];
        int temp_parent1 = find(parent,temp_edge.first-1);
        int temp_parent2 = find(parent,temp_edge.second-1);
        if(temp_parent1 == temp_parent2)
        {
            cout<<"Cycle"<<endl;
            break;
        }
        Union(parent,temp_parent1,temp_parent2);
    }
    cout<<"no Cycle"<<endl;
}