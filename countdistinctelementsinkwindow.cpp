#include<bits/stdc++.h>

//count number of distinct elements in window of size k in an array 

using namespace std;

int main(){
    int test1;
    cin>>test1;
    while(test1--){
        int number,window;
        cin>>number>>window;
        vector<int> arr;
        for(int i=0;i<number;i++){
            int temp;
            cin>>temp;
            arr.push_back(temp);
        }
        unordered_map<int,int> mt;
        for(int i=0;i<=number-window;i++){
            if(mt.empty()){
                for(int j=i;j<i+window;j++){
                    mt[arr[j]]+=1;
                }
                cout<<mt.size()<<" ";
            }
            else
            {
                if(mt[arr[i-1]] > 1){
                    mt[arr[i-1]]--;
                }
                else
                {
                    mt.erase(mt.find(arr[i-1]));
                }
                
                mt[arr[i+window-1]] = 1;
                cout<<mt.size()<<" ";
            }

        }
    cout<<endl;
    }
}