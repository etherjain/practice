//TODO

#include<bits/stdc++.h>


using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}


void travelleft(Node* root){
    if(root == NULL){
        return;
    }
    if(root->left){
        cout<<root->data<<" ";
        travelleft(root->left);
    }
    else if(root->right){
        cout<<root->data<<" ";
        travelleft(root->right);
    }
    return ;
}
void travelright(Node* root){
    if(root == NULL){
        return;
    }
    if(root->right){
        travelright(root->right);
        cout<<root->data<<" ";
        
    }
    else if(root->left){
        travelright(root->left);
        cout<<root->data<<" ";
       
    }
    return ;
}
void travelleaf(Node* root){
    if(root == NULL){
        return ;
    }
    if(root->left !=NULL){
        travelleaf(root->left);
    }

    if (root->left == NULL && root->right == NULL){
        cout<<root->data<<" ";
    }

    if(root->right != NULL){
        travelleaf(root->right);
    }
    return;
}

void boundarytravesal(Node* root){
    if(root == NULL){
        return;
    }
    cout<<root->data<<" ";
    travelleft(root->left);
    travelleaf(root->left);
    travelleaf(root->right);
    travelright(root->right);
}

int main(){
    Node* root = addNode(20); 
    root->left = addNode(8); 
    root->left->left = addNode(4); 
    root->left->right = addNode(12); 
    root->left->right->left = addNode(10); 
    root->left->right->right = addNode(14); 
    root->right = addNode(22); 
    root->right->right = addNode(25);
    boundarytravesal(root);
    return 0;
}


