#include <iostream>
#include<bits/stdc++.h>
#include<vector>
using namespace std;

int multiply(int number,vector<int> number2,int last_index)
{
    vector<int> temp = number2;
    int carry_on  = 0;
    for(int i=0;i<=last_index;i++){
        int value = number2[i]*number+carry_on;
        number2[i] = value%10;
        carry_on = value/10;
    }
    while(carry_on){
        number2[++last_index] = carry_on%10;
        carry_on/=10;
    }
    return last_index;
    
}


int main() {
    
    int number1,number2;
    cin>>number1>>number2;
    vector<int> fac1(INT_MAX);
    vector<int> fac2(INT_MAX);
    fac1[0]=1;
    int last_index = 0;
    for(int i=1;i<=number1;i++){
        last_index = multiply(i,fac1,last_index);
    }
    
    for(int i=0;i<=last_index;i++){
        cout<<fac1[i]<<" ";
    }
    cout<<endl;
    
	//code
	return 0;
}