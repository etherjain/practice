#include<bits/stdc++.h>

//smallest window in a string that contains all the characters of the patter.

using namespace std;

int main(){
    string s;
    cin>>s;
    string pat;
    cin>>pat;
    unordered_map<int,int> mt;
    for(int i=0;i<pat.length();i++){
        mt[int(pat[i])-int('a')]++;
    }
    int pat_coun = pat.length();
    int count=0;
    int char_start = 0;
    int char_end = 0;
    unordered_map<int,int> visited;
    for(int i=0;i<s.length();i++){
        int char_num = int(s[i]) - int('a');
        if(mt.find(char_num) != mt.end()){
            if(count ==0){
                char_start = i;
                count++;
                char_end = i;
                visited[char_num]++;
            }
            else
            {
                if(visited.find(char_num) == visited.end()){
                    count++;
                 char_end=  i;
                 visited[char_num]++;
                }
                else{
                    visited[char_num]++;
                    char_end++;
                }
            }
        }
        if(count == pat_coun){
            break;
        }
    }
    if(count == pat_coun){
        cout<<"Char start"<<char_start<<" char_end "<<char_end<<endl;
        while(char_start < char_end){
            int char_num = int(s[char_start])- int('a');
            if(visited.find(char_num) != visited.end()){
                if(visited[char_num] > 1 && visited[char_num] > mt[char_num]){
                    char_start ++ ;
                    visited[char_num]--;
                }
                else{
                    break;
                }
            }
            else
            {
                char_start++;
            }
        }
        string result;
        for(int i=char_start;i<=char_end;i++){
            result+=s[i];
        }
        cout<<"Result "<<result<<endl;

    }
    else{
        cout<<"Count"<<count;
        cout<<"No match"<<endl;
    }
}
