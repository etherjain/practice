#include<bits/stdc++.h>

int searchAlmostSorted(std::vector<int> arr,int l,int r,int key)
{
    if(l<=r)
    {
        int mid= (l+r)/2;
        if(arr[mid]==key)
        {
            return mid;
        }
        if(l<mid && arr[mid-1]==key){
            return mid-1;
        }
        if(r>mid && arr[mid+1]==key)
        {
            return mid+1;
        }

        if(arr[mid]>key)
        {
            return searchAlmostSorted(arr,l,mid-2,key);

        }
        else
        {
            return searchAlmostSorted(arr,mid+2,r,key);
        }
    }
}

int main()
{
    int test;
    std::cin>>test;
    while(test--)
    {
        int number;
        std::cin>>number;
       std::vector<int> arr;
        for(int i=0;i<number;i++)
        {
            int temp;
            std::cin>>temp;
            arr.push_back(temp);
        }
        int value;
        std::cin>>value;
        std::cout<<"value "<<searchAlmostSorted(arr,0,arr.size()-1,value)<<std::endl;
    }
    return 0;
}