#include<bits/stdc++.h>

using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
};

Node* addNode(int data){
    Node* temp = new Node;
    temp->data=  data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

Node* insert(Node* root,int key){
    if(root == NULL){
        return addNode(key);
    }
    if(root->data > key){
        root->right = insert(root->right,key);
    }
    else{
        root->left = insert(root->left,key);
    }
    return root;
}
void greatSumTree(Node* root,int &value){
    if(root == NULL){
        return;
    }
    greatSumTree(root->right,value);
    root->data = root->data + value;
    value = root->data;
    greatSumTree(root->left,value);
}

int main(){

    Node* root = addNode(4);
    root->left = addNode(1);
    root->right = addNode(6);
    root->left->left = addNode(0);
    root->left->right = addNode(2);
    root->left->right->right = addNode(3);
    root->right->left = addNode(5);
    int sum = 0;
    greatSumTree(root,sum);
}