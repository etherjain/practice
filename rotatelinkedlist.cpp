#include<bits/stdc++.h>


using namespace std;

struct Node{
    int data;
    Node* next;
};

struct Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->next = NULL;
    return temp;
}

int getLinkedlistlenght(Node* head){
    Node* temp = head;
    int count = 0;
    while(temp!=NULL){
        count ++;
        temp = temp->next;
    }
    return count ;
}
Node* rotateLinkedlist(Node* head,int number){
    int llhieght = getLinkedlistlenght(head);
    int rotate = number%llhieght;
    if(rotate == 0){
        return head;
    }
    Node* temp_head = head;
    Node* temp_head2 = head;
    for(int i=0;i<rotate-1;i++){
        temp_head = temp_head->next;
    }
    Node* prev_head = temp_head2;
    while(temp_head->next!=NULL){
        prev_head = temp_head2;
        temp_head2 = temp_head2->next;
        temp_head= temp_head->next;
    }
    temp_head->next = head;
    prev_head->next = NULL;
    return temp_head2;
}

void printNode(Node* head){
    Node* temp =head;
    while(temp!=NULL){
        cout<<temp->data<<" ";
        temp =temp->next;
    }
    cout<<endl;   
}

int main(){
    Node* head = addNode(10);
    head->next = addNode(20);
    head->next->next = addNode(30);
    head->next->next->next = addNode(40);
    // head->next->next->next->next = addNode(50);
    Node* temp_head = rotateLinkedlist(head,3);
    printNode(temp_head);
    return 0;
}