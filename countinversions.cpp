#include<bits/stdc++.h>


using namespace std;



int merge(int *arr,int low,int mid,int high,int size){
    int inversion_count = 0;
    int i = low;
    int j = mid;
    int temp[size];
    for(int l=0;l<size;l++){
        temp[l] = arr[l];
    }
    int k=i;
    while(i<=mid-1 && j<=high){
        if(arr[i] <= arr[j]){
            temp[k++] = arr[i];
            i++;
        }
        else
        {
            temp[k++] = arr[j];
            j++;
            inversion_count+=mid-i;
        }
    }
    while(i<=mid-1){
        temp[k++]= arr[i];
        i++;
    }
    while(j<=high){
        temp[k++] = arr[j];
        j++;
    }
    for(int i= low;i<=high;i++){
        arr[i] = temp[i];
    }
    return inversion_count;
}

int merge_sort(int *arr,int low,int high,int size){
    if(low<high){
        int mid = (low+high)/2;

        int l = merge_sort(arr,low,mid,size);
        // cout<<"mid "<<mid<<"low "<<l<<endl;
        int r = merge_sort(arr,mid+1,high,size);
        // cout<<"mid "<<mid<<"right "<<r<<endl;
        return l+r+merge(arr,low,mid+1,high,size);
    }
    else
    return 0;
}

int main(){
    int number;
    cin>>number;
    int arr[number];
    for(int i=0;i<number;i++){
        cin>>arr[i];
    }
    cout<<"inversion_count "<<merge_sort(arr,0,number-1,number);
    return 0;
}