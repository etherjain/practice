//find number of islandsin a graph 

#include<bits/stdc++.h>

using namespace std;

int visited[10000] = {0};

int graph[10000][10000] = {0};

void dfs(int start,int vertices){

    stack<int> st;
    st.push(start);
    while(!st.empty()){
        int temp_node = st.top();
        // cout<<"Temp Node "<<temp_node<<endl;
        visited[temp_node]=1;
        st.pop();
        for(int i=1;i<=vertices;i++){
            // cout<<temp_node<<" "<<i<<endl;
            // cout<<graph[temp_node][i]<<endl;
            if(graph[temp_node][i] == 1){
                // cout<<"I th "<<i<<endl;
                if(visited[i] == 0){
                    st.push(i);
                }
            }   
        }
    }
}

int get_island(int vertices){
    int count = 0 ;

    for(int i=1;i<=vertices;i++){
        if(visited[i] == 0){
            dfs(i,vertices);
            count++;
        }
    }
    return count;
}

// int M[][COL] = { { 1, 1, 0, 0, 0 }, 
//                      { 0, 1, 0, 0, 1 }, 
//                      { 1, 0, 0, 1, 1 }, 
//                      { 0, 0, 0, 0, 0 }, 
//                      { 1, 0, 1, 0, 1 } }; 

int main(){

    int vertices,edges;
    cin>>vertices>>edges;
    for(int i=0;i<edges;i++){
        int node1,node2;
        cin>>node1>>node2;
        graph[node1][node2] = 1;
        graph[node2][node1] = 1;
    }
    // for(int i=1;i<=vertices;i++){
    //     for(int j=1;j<=vertices;j++){
    //         cout<<graph[i][j]<<" ";
    //     }
    //     cout<<endl;
    // }
    cout<<"Islands "<<get_island(vertices)<<endl;
}

//1-2
//1-3 
//4-5
//4-6 