#include<bits/stdc++.h>

using namespace std;

#define MAX_CHAR 26
struct TrieNode{
    int endofword = false;
    TrieNode* children[MAX_CHAR];
};


TrieNode* getNode()
{
    TrieNode* newNode = new TrieNode;
    newNode->endofword = false;
    for(int i =0;i<MAX_CHAR;i++){
        newNode->children[i] = NULL;
    }
    return newNode;    
}

void insert(TrieNode* root,string key){
    if(root==NULL)
    {
        return;
    }
    TrieNode* temp = root;
    for(int i=0;i<key.length();i++){
        int index = int(key[i]) - int('a');
        if(temp->children[index] == NULL){
            temp->children[index] = getNode();
            temp = temp->children[index];  
        }else
        {
            temp = temp->children[index];
        }
    }
    temp->endofword = true;
}

bool search(TrieNode* root,string key){
    if(root ==  NULL){
        return false;
    }
    if(key.length() == 0){
        return false;
    }
    TrieNode* temp = root;
    for(int i=0;i<key.length();i++){
        int index = int(key[i]) -int('a');
        if(temp->children[index] == NULL){
            return false;
        }
        temp = temp->children[index];
    }
    return (temp!=NULL && temp->endofword);
}

bool isEmpty(TrieNode* root){
    for(int i=0;i<MAX_CHAR;i++){
        if(root->children[i]){
            return false;
        }
    }
    return true;
}
TrieNode* remove(TrieNode* root,string key,int depth = 0){
    if(root == NULL){
        return NULL;
    }
    if(key.length() == 0){
        return root;
    }
    if(depth == key.length()){
        if(root->endofword){
            root->endofword = false;
        }
        if(isEmpty(root)){
            delete(root);
            root = NULL;
        }
        return root;
    }
    int index = key[depth] - 'a';
    root->children[index] = remove(root->children[index],key,depth+1);
    
    if(isEmpty(root) && root->endofword == false){
        delete(root);
        root = NULL;
    }
    return root;
}
int main() 
{ 
    // Input keys (use only 'a' through 'z' 
    // and lower case) 
    string keys[] = { "the", "a", "there", 
                      "answer", "any", "by", 
                      "bye", "their", "hero", "heroplane" }; 
    int n = sizeof(keys) / sizeof(keys[0]); 
  
    struct TrieNode* root = getNode(); 
  
    // Construct trie 
    for (int i = 0; i < n; i++) 
        insert(root, keys[i]); 
  
    // Search for different keys 
    search(root, "the") ? cout << "Yes\n" : cout << "No\n"; 
    search(root, "these") ? cout << "Yes\n" : cout << "No\n"; 
  
    remove(root, "heroplane"); 
    search(root, "hero") ? cout << "Yes\n" : cout << "No\n"; 
    return 0; 
} 