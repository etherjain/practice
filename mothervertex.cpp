#include<bits/stdc++.h>
using namespace std;

vector<int> graph[100000];
int visited[100000]={0};

void dfs_util(int node)
{
    stack<int> s;
    s.push(node);
    visited[node]=1;
    while(!s.empty())
    {
        int temp = s.top();
        s.pop();
        cout<<temp<<endl;
        for(int i=0;i<graph[temp].size();i++)
        {
            if(!visited[graph[temp][i]])
            {
                s.push(graph[temp][i]);
                visited[graph[temp][i]]=1;
            }
        }
    }
}

int dfs(int vertices)
{
    int count =0;
    int mother_node=0;
    for(int i=0;i<vertices;i++)
    {
        if(visited[i]==0)
        {
            dfs_util(vertices);
            mother_node=i;
        }
    }
    
    memset(visited,0,sizeof(visited)/sizeof(int));

    dfs_util(mother_node);
    for(int i=0;i<vertices;i++)
    {
        if(visited[i]==0)
        {
            return -1;
        }
    }
    return mother_node;
}

int main()
{
    int test;
    cin>>test;
    while(test--)
    {
        int vertices,edges;
        cin>>vertices>>edges;
        for(int i=0;i<edges;i++)
        {
            
        }
    }
}
