#include<bits/stdc++.h>

using namespace std;

#define PII pair <long long, int > 

const int MAX  = 1e4+5;
vector<PII> adj[MAX];
int visited[MAX];



void prims(int x){
    priority_queue<PII,vector<PII>,greater<PII> > Q;
    Q.push(make_pair(0,x));
    int count = 0;
    while(!Q.empty()){
        PII temp = Q.top();
        Q.pop();
        int temp_node = temp.second;
        if(visited[temp_node]){
            continue;
        }
        visited[temp_node]= 1;
        count+=temp.first;
        for(int i=0;i<adj[temp_node].size();i++){
            if(visited[adj[temp_node][i].second] == 0)
            {

                Q.push(adj[temp_node][i]);
            }
        }
    }
    cout<<count<<endl;
}

int main(){
    int vertices,edges;
    memset(visited,0,sizeof(visited));
    cin>>vertices>>edges;
    for(int i=0;i<edges;i++){
        int temp1,temp2,value;
        cin>>temp1>>temp2>>value;
        adj[temp1].push_back(make_pair(value,temp2));
        adj[temp2].push_back(make_pair(value,temp1));
    }
    prims(1);
    return 0;
}