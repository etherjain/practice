#include<bits/stdc++.h>


using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
    int height;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

void printLeft(Node* root){
    if(root == NULL){
        return;
    }
    queue<Node*> qt ;
    root->height = 0;
    int max_height = -1;
    qt.push(root);
    while(!qt.empty())
    {
        Node* temp = qt.front();
        qt.pop();
        int temp_height = temp->height;
        if(temp_height > max_height){
            cout<<temp->data<<" ";
            max_height = temp_height;
        }
        if(temp->left != NULL){
            temp->left->height = temp_height + 1;
            qt.push(temp->left);
        }
        if(temp->right != NULL){
            temp->right->height = temp_height + 1;
            qt.push(temp->right);
        }
    }
    return;
}

int main(){
    Node* root = addNode(10);
    root->left = addNode(9);
    root->right = addNode(20);
    root->right->left = addNode(19);
    root->right->right = addNode(21);
    printLeft(root);
    return 0;
}