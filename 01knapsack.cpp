#include<bits/stdc++.h>

using namespace std;


int main(){
    int items;
    int max_weight;
    cin>>items;
    cin>>max_weight;
    vector<int> values;
    vector<int> weights;
    for(int i=0;i<items;i++){
        int temp_value;
        int temp_w;
        cin>>temp_value>>temp_w;
        values.push_back(temp_value);
        weights.push_back(temp_w);
    }
    int sack[items+1][max_weight+1];
    memset(sack,0,sizeof(sack));
    for(int i=0;i<=items;i++){
        for(int w=0;w<=max_weight;w++){
            if (i==0 || w ==0 ){
                sack[i][w] == 0;
            }
            else if (w >= weights[i-1]){
                sack[i][w] = max(values[i-1]+sack[i-1][w-weights[i-1]],sack[i-1][w]);
            }
            else
            {
                sack[i][w] = sack[i-1][w];
            }
        }
    }
    cout<<"Max value "<<sack[items][max_weight];
    return 0;
}
