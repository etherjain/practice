#include<bits/stdc++.h>
//Middle Element 
//reverse a linkedlist
using namespace std;

struct Node{
    int data;
    Node* next;
};

struct Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->next = NULL;
    return temp;
}

void middleElement(struct Node* root){
    if(root == NULL){
        return;
    }
    Node* temp1 = root; 
    Node* temp2 = root;

    while(temp2!= NULL){
        if(temp2->next != NULL && temp2->next->next!=NULL){
            temp2 = temp2->next->next;
            temp1 = temp1->next;
        }
        else if (temp2->next == NULL){
            cout<<"Middle el"<<temp1->data<<endl;
            temp2 = NULL;
            break;
        }
        else if(temp2->next!= NULL && temp2->next->next == NULL){
            temp2 = NULL;
            temp1 = temp1->next;
            cout<<"Middle el"<<temp1->data<<endl;
        }
    }
    return;
}
Node* reverseLinkedList(Node* root){
    Node* temp = root;
    Node* temp2 = NULL;
    while(temp!=NULL){
        Node* temp3 = temp->next;
        temp->next = temp2;
        temp2 = temp;
        temp = temp3;
    }
    return temp2;
}

void printNode(Node* head){
    Node* temp =head;
    while(temp!=NULL){
        cout<<temp->data<<" ";
        temp =temp->next;
    }
    cout<<endl;   
}

Node* pairwiseswap(Node* head){
    if(head == NULL){
        return NULL;
    }
    if(head->next == NULL){
        return head;
    }
    Node* temp_ptr1 = head;
    Node* prev_ptr = head;
    Node* temp_head = head->next;
    while(temp_ptr1 && temp_ptr1->next!=NULL){
        prev_ptr->next = temp_ptr1->next;
        Node* temp2 = temp_ptr1->next->next;
        temp_ptr1->next->next = temp_ptr1;
        temp_ptr1->next = temp2;
        prev_ptr = temp_ptr1;
        temp_ptr1 = temp_ptr1->next;  
    }
    return temp_head;
}
int main(){
    Node* head = addNode(10);
    head->next = addNode(20);
    head->next->next = addNode(30);
    head->next->next->next = addNode(40);
    head->next->next->next->next = addNode(50);
    Node* temp_head = pairwiseswap(head);
    printNode(temp_head);
    // middleElement(head);
}