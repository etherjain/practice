#include<bits/stdc++.h>

using namespace std;


int main(){

    int r,c;
    cin>>r>>c;

    int arr[r][c];

    for(int i=0;i<r;i++){
        for(int j=0;j<c;j++){
            cin>>arr[i][j];
        }
    }

    //preprocess step 
    int aux[r][c];

    memset(aux,0,sizeof(aux));

    //initializing first row
    for(int i=0;i<c;i++){
        aux[0][i] = arr[0][i];
    }

    //doing row wise sum
    for(int i=1;i<r;i++){
        for(int j=0;j<c;j++){
            aux[i][j] = arr[i][j]+aux[i-1][j];
        }
    }
    //doing colom wise sum 

    for(int i=0;i<r;i++){
        for(int j=1;j<c;j++){
            aux[i][j] = aux[i][j-1]+aux[i][j];
        }
    }

    int queries;
    cin>>queries;
    while(queries--){
        int result;
        int top,left,bottom,right;
        cin>>top>>left>>bottom>>right;
        result =  aux[bottom][right];

        if(left > 0){
            result =result - aux[bottom][left-1];
        }
        if(top > 0){
            result = result - aux[top-1][right];
        }
        if(top > 0 && left > 0){
            result = result + arr[top-1][left-1];
        }
        cout<<"result "<<result;
    }
    return 0;
}