#include<bits/stdc++.h>

using namespace std;


int main(){

    int number;
    cin>>number;
    vector<int> arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }

    int lis[number];
    memset(lis,0,sizeof(lis));
    lis[0] = 1;
    for(int i=1;i<number;i++){
        lis[i] = 1;
        for(int j=0;j<i;j++){
            if(arr[i] > arr[j] && lis[i] < lis[j]+1)
            {
                lis[i] = lis[j]+1;
            } 
        }
    }

    int max_len = *max_element(lis,lis+number);
    cout<<max_len<<endl;
    return 0;
}   