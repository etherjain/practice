#include "point.h"
// using namespace std;

int orientation(Point p1,Point p2,Point p3)
{
    // float angle1 = (p2.y-p1.y)/(p2.x-p1.x);
    // float angle2 = (p3.y-p2.y)/(p3.x-p2.x);

    float value = (p2.y-p1.y)*(p3.x-p2.x)-(p3.y-p2.y)*(p2.x-p1.x);
    if(value <  0 )
    {
        return -1;
    }
    else if(value > 0){
        return 1;

    }else
    {
        return 0;

    }
}

int check_colinear_intersect(Point p1,Point q1,Point p2)
{
    if(q1.x >= std::min(p1.x,p2.x) && q1.x <= std::max(p1.x,p2.x) && q1.y >= std::min(p1.y,p2.y) && q1.y <= std::max(p1.y,p2.y))
    {
        return 1;
    }
    return 0;
}

int check_rotation(Point p1,Point p2,Point q1,Point q2)
{
    
    int value1 = orientation(p1,q1,p2);

    int value2 = orientation(p1,q2,p2);
    int value3 = orientation(q1,p1,q2);
    int value4 = orientation(q1,p2,q2);
    if(value1 != value2 && value3!=value4)
    {
        return 1;
    }
    //if p1 ,p1 and q1 are colinear then check if q1 is in the segment p1,p2
    if(value1 == 0  && check_colinear_intersect(p1,q1,p2))
    {
        return 1;
    }
    if(value2 == 0 && check_colinear_intersect(p1,q2,p2))return 1;
    if(value3 == 0 && check_colinear_intersect(q1,p1,q2))return 1;
    if(value4 ==0 && check_colinear_intersect(q1,p2,q2))return 1;

    return 0;

}
// int main(){
//     Point p1 = {0,0};
//     Point p2 = {4,4};
//     Point p3 = {1,2};
//     cout<<orientation(p1,p2,p3)<<endl;
//     return 0;
// }