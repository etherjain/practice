#include<bits/stdc++.h>
using namespace std;


void insertionsort(vector<int> &arr){
    for(int i=1;i<arr.size();i++){
        int key = arr[i];
        int j = i-1;
        while(j>=0 && arr[j] > key){
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}

int main(){
    int number;
    cin>>number;
    vector<int> arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }
    insertionsort(arr);
    for(int i=0;i<number;i++){
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}