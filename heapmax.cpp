#include<bits/stdc++.h>

using namespace std;

#define MAX 1000
struct heap{
    int arr[MAX];
    int heapsize;
    heap(){
        heapsize = -1;
    }
    void maxheapify(int index);
    void insert(int data);
    void update(int data,int index);
    void deletelement(int index);
    int getmax(){
        return arr[0];
    };
    int extractmax();    
};

void heap::maxheapify(int index){
    int left = 2*index + 1;
    int right = 2*index + 2;
    int largest = index;
    if(left<=heapsize && arr[index] < arr[left]){
        largest = left;
        swap(arr[index],arr[left]);
    }
    if(right <= heapsize && arr[index] < arr[right]){
        largest = right;
        swap(arr[right],arr[index]);
    }
    if(largest  != index){
        maxheapify(largest);
    }
}

int heap::extractmax(){
    if(heapsize > -1){
        int max_el = arr[0];
        arr[0] = arr[heapsize];
        heapsize--;
        maxheapify(0);
        return max_el;
    }
}
void heap::deletelement(int index){

    if(index < 0 || index > heapsize){
        return;
    }
    else
    {
        arr[index] = INT_MAX;
        while(index !=0 && arr[index] > arr[(index-1)/2])
        {
            swap(arr[index],arr[(index-1)/2]);
            index = (index-1)/2;
        }
        extractmax();
    }
}

void heap::update(int data,int index){
    if(index < 0 ||index > heapsize){
        return;
    }
    else{
        arr[index] = data;
        while(index!=0 && arr[index] > arr[(index-1)/2]){
            swap(arr[index],arr[(index-1)/2]);
            index = (index-1)/2;
        }
        if(index!=0){
            maxheapify(index);
        }
    }
}

void heap::insert(int data){
    if(heapsize < MAX-1){
        heapsize++;
        arr[heapsize] = data;
        int index = heapsize;
        while(index!=0  && arr[index] > arr[(index-1)/2]){
            swap(arr[index],arr[(index-1)/2]);
            index = (index-1)/2;
        }
    }
    else{
        cout<<"heap full"<<endl;
    }
}

int main(){
    heap h = heap(); 
    h.insert(3); 
    h.insert(2); 
    h.deletelement(1); 
    h.insert(15); 
    h.insert(5); 
    h.insert(4); 
    h.insert(45); 
    cout << h.extractmax() << " "; 
    cout << h.getmax() << " "; 
    h.update(2, 1); 
    cout << h.getmax();
return 0;
}
