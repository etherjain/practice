#include<bits/stdc++.h>

using namespace std;


int main(){
    int test;
    cin>>test;
    while(test--){
        vector<int> arr;
        int number;
        cin>>number;
        for(int i=0;i<number;i++){
            int temp;
            cin>>temp;
            arr.push_back(temp);
        }

        int curr_max = arr[0];
        int curr_max_index = 0;
        int change_flag = 1;
        int temp_max = arr[0];
        int temp_index = 0;
        for(int i=1;i<number;i++){

            if(arr[i] > curr_max){
                if(change_flag == 1 && i!=number-1){
                    curr_max = arr[i];
                    curr_max_index = i;
                    change_flag =0 ;
                    temp_max = arr[i];
                    temp_index = i;
                }
                else{
                    temp_max = arr[i];
                    temp_index = i;
                }
                
            }
            else{
                if(change_flag == 0){
                    change_flag = 1;
                    curr_max = temp_max;
                    curr_max_index = temp_index;
                }
            }
        }

        if(change_flag == 1){
            cout<<"-1"<<endl;
        }
        else
        {
            cout<<curr_max<<endl;
        }
    }

    return 0;
}