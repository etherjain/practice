#include<bits/stdc++.h>
using namespace std;
bool check(vector<int> arr1,vector<int> arr2,int diff){
    unordered_map<int,int> mt;
    for(int i=0;i<arr1.size();i++){
        mt[arr1[i]] = 1;
    }
    int flag = false;
    for(int i=0;i<arr2.size();i++){
        if (mt.find(arr2[i]+diff) != mt.end()){
            return true;
            break;
        }
    }
    return flag;
}
int main() {
    
    int test;
    cin>>test;
    while(test--){
        int number1,number2;
        cin>>number1>>number2;
        vector<int> arr1;
        vector<int> arr2;
        for(int i=0;i<number1;i++){
            int temp;
            cin>>temp;
            arr1.push_back(temp);
        }
        for(int i=0;i<number2;i++){
            int temp;
            cin>>temp;
            arr2.push_back(temp);
        }
        
        int sum1 =0;
        int sum2 = 0;
        for(int i=0;i<number1;i++){
            sum1 +=arr1[i];
        }
        for(int i=0;i<number2;i++){
            sum2 +=arr2[i];
        }
        if(sum1 == sum2){
            cout<<-1<<endl;
        }
        if(sum1 > sum2){
            int diff = sum1-sum2;
            diff = floor(diff / 2);
            if(check(arr1,arr2,diff)){
                cout<<1<<endl;}
            else{
                cout<<-1<<endl;}
        }
        else{
            int diff = sum2-sum1;
            diff = floor(diff / 2);
            if(check(arr2,arr1,diff)){
                cout<<1<<endl;
            }
            else{
                cout<<-1<<endl;
            }
        }
        
    }
	//code
	return 0;
}