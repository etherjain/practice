#include<bits/stdc++.h>

using namespace std;

int main()
{
    string s;
    cin>>s;
    int bitCounter = 0;
    for(int i=0;i<s.length();i++)
    {
        int temp = int(s[i]-'a');
        int mask = 1<<temp;
        if((bitCounter&mask ) == 0)
        {
            bitCounter|=mask;
        }
        else{
            bitCounter&=~mask;
        }
    }
    if((bitCounter & bitCounter-1)==0)
    {
        cout<<"Permutation"<<endl;
    }
    else
    {
        cout<<"false"<<endl;
    }
    return 0;
}