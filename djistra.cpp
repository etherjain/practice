#include<bits/stdc++.h>

using namespace std;

#define MAX_ 10000

vector< pair<int,int> > graph[MAX_];
bool vis[MAX_];
int dis[MAX_];

void dij(int k,int vertices){
    multiset<pair<int,int>> s;
    memset(vis,false,sizeof(vis));
    memset(dis,MAX_,sizeof(dis));
    dis[k]=0;
    s.insert(make_pair(0,k));
    while(!s.empty()){
        pair<int,int> temp = *s.begin();
        s.erase(*s.begin());
        int x = temp.second;
        int w = temp.first;
        if(vis[x]) continue;

        vis[x] = true;
        for(int i=0;i<graph[x].size();i++)
        {   
            int temp1 = graph[x][i].first;
            int weight = graph[x][i].second;
            if(dis[x]+weight<dis[temp1])
            {
                dis[temp1] = dis[x]+weight;
            }
            s.insert(make_pair(weight,temp1));
        }   
    }
    for(int i=1;i<=vertices;i++){
        cout<<dis[i]<<" ";
    }
    cout<<endl;
}
int main(){

    int vertices,edeges;
    cin>>vertices>>edeges;
    for(int i=0;i<edeges;i++)
    {
        int temp1,temp2,weight;
        cin>>temp1>>temp2>>weight;
        graph[temp1].push_back(make_pair(temp2,weight));
        graph[temp2].push_back(make_pair(temp1,weight));
    }
    int startingvertex ;
    cin>>startingvertex;
    dij(startingvertex,vertices);
    return 0;
}
