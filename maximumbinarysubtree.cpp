#include<bits/stdc++.h>
//create a tree using the vector of numbers given with max number is the root and the left and right trees are there also 
using namespace std;

struct Node{
    int data;
    Node* left;
    Node* right;
};

Node* addNode(int data){
    Node* temp = new Node;
    temp->left = NULL;
    temp->right = NULL;
    temp->data = data;
    return temp;
}
Node* createTree(vector<int> arr, int l, int r){
    if(l<=r){
        vector<int>::iterator max_it;
        max_it = max_element(arr.begin()+l,arr.begin()+r+1);
        int index  = max_it - arr.begin();
        cout<<"Index "<<index<<endl;
        int max_el = *max_it;
        cout<<"Max_el "<<max_el<<endl;
        Node* temp_node = addNode(max_el);
        temp_node->right = createTree(arr,index+1,r);
        temp_node->left = createTree(arr,l,index-1);
        return temp_node;
    }
    return NULL;
}

void preOrder(Node* root){
    if(root == NULL){
        return;
    }
    cout<<root->data<<" ";
    preOrder(root->left);
    preOrder(root->right);
}

int main(){
    int number;
    cin>>number;
    vector<int>arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }
    Node* tree = createTree(arr,0,arr.size()-1);
    preOrder(tree);
    cout<<endl;

}