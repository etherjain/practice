#include<bits/stdc++.h>

using namespace std;


int main()
{
    string s;
    cin>>s;

    int visited[256]={-1};
    int length = s.length();
    visited[int(s[0])] = 0;
    int curr_len = 1;
    int max_len = 1;
    for(int i=1;i<length;i++)
    {
        int previous_index = visited[int(s[i])];
        if(previous_index == -1 || i - curr_len > previous_index)
        {
            curr_len++;
        }
        
        else
        {
            if(curr_len > max_len)
            {
                max_len = curr_len;
            }
            curr_len = i - previous_index;
        }
        visited[int(s[i])] = i;
    }
    cout<<curr_len<<endl;
    return 0;
}