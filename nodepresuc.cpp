// print predicisor and succesor of a node in bst. if not present return the nodes in between which it lies


#include<bits/stdc++.h>


using namespace std;


struct Node{
    int data;
    Node* right;
    Node* left;
};


Node* addNode(int data){
    Node* newNode = new Node;
    newNode->data = data;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}


void getPredSuc(Node* root, Node* &pred, Node* &suc,int key){
    if (root == NULL){
        return ;
    }

    if(root->data == key){
        Node* rightNode;
        rightNode = root->right;
        while(rightNode != NULL){
            suc = rightNode;
            rightNode = rightNode->left;
        }
        
        Node* leftNode;
        leftNode = root->left;
        while(leftNode!=NULL){
            pred = leftNode;
            leftNode = leftNode->right;
        }
        return;
    }

    if(root->data > key){
        suc = root;
        getPredSuc(root->left, pred, suc, key);
    }
    else{
        pred = root;
        getPredSuc(root->right,pred,suc,key);
    }
}


int main(){
    Node* root = addNode(50);
    root->left = addNode(30);
    root->right = addNode(70);
    root->left->right = addNode(40);
    root->left->left = addNode(20);
    root->right->left = addNode(60);
    root->right->right = addNode(80);
    Node* pred=NULL;
    Node* suc=NULL;
    getPredSuc(root,pred,suc,60);
    cout<<"Completed pred suc"<<endl;
    if (pred != NULL){
        cout<<pred->data<<endl; 
    }
    if(suc !=NULL){
        cout<<suc->data<<endl;
    }    
    return 0;
}