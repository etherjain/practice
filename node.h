#ifndef NODE_H
#define NODE_H

struct Node{
    int data;
    Node* next;
};

Node* addNode(int data);
Node* addNodes(int number);
void printNode(Node* head);


#endif