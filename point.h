#ifndef POINT_H
#define POINT_H

#include<vector>
#include<iostream>
#include<stdlib.h>
#include<algorithm>
struct Point{
    int x;
    int y;
};

int orientation(Point p1,Point p2,Point p3);
int check_rotation(Point p1,Point p2,Point p3,Point p4);
int check_colinear_intersect(Point p1,Point q1,Point p2);


#endif