#include<bits/stdc++.h>
///Find least common ancestor of two elements in a tree. 


using namespace std;


struct Node{
    int data;
    Node* right;
    Node* left;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right =  NULL;
    return temp;
}

Node* lca(Node *root,int key1,int key2,int &v1,int &v2){

    if(root == NULL){
        return NULL;
    }

    if(root->data == key1){
        v1 = 1;
        return root;
    }
    if(root->data == key2){
        v2 = 1;
        return root;
    }
    Node* left_lca = lca(root->left,key1,key2,v1,v2);
    Node* right_lca= lca(root->right,key1,key2,v1,v2);
    if(right_lca && left_lca){
        return root;
    }
    
    return left_lca!=NULL?left_lca:right_lca;
}

bool find(Node* root,int key){
    if(root == NULL){
        return false;
    }
    if(root->data == key){
        return true;
    }
    else{
        return find(root->left,key) || find(root->right,key);
    }
}

Node* findLCA(Node* root,int key1,int key2){
    int v1 =0;
    int v2 =0;
    Node* result = lca(root,key1,key2,v1,v2);
    if((v1&&v2) || (v1 && find(result,key2)) || (v2 && find(result,key1))){
        return result;
    }
    else
    {
        return NULL;
    }
}


int main(){
    Node * root = addNode(1); 
    root->left = addNode(2); 
    root->right = addNode(3); 
    root->left->left = addNode(4); 
    root->left->right = addNode(5); 
    root->right->left = addNode(6); 
    root->right->right = addNode(7); 
    Node* lca2 = findLCA(root,3,1);
    if(lca2 != NULL){
        cout<<"LCA "<<lca2->data<<endl;
    }
    else{
        cout<<"LCA NULL"<<endl;
    }
    return 0;
}
