#include "node.h"
#include<iostream>
using namespace std;

Node* addNode(int data)
{
    Node* temp = new Node;
    temp->data = data;
    temp->next= NULL;
}
Node* addNodes(int number)
{
    Node* head = NULL;
    Node* tempNode = NULL;
    for(int i=0;i<number;i++){

        int temp;
        cin>>temp;
        
        if(head ==NULL)
        {
            head = addNode(temp);
            tempNode = head;
        }
        else{
            
            tempNode->next = addNode(temp);
            tempNode = tempNode->next;
        }
    }
    return head;
}
void printNode(Node* head)
{
    while(head!=NULL)
    {
        cout<<head->data<<" ";
        head = head->next;
    }
    cout<<endl;
}