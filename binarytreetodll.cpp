#include<bits/stdc++.h>

using namespace std;

struct Node{
    int data;
    Node* left;
    Node* right;
};

Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

Node* binarytoDll(Node* root,Node **head,Node **prev){
    if(root == NULL){
        return NULL;
    }

    binarytoDll(root->left,head,prev);

    if((*prev) == NULL){
        *head = root;
        *prev = root;
    }
    else{
        root->left = *prev;
        (*prev)->right = root;
        *prev = root;
    }
    binarytoDll(root->right,head,prev);
}

int main(){
    Node *root        = addNode(10); 
    root->left        = addNode(12); 
    root->right       = addNode(15); 
    root->left->left  = addNode(25); 
    root->left->right = addNode(30); 
    root->right->left = addNode(36); 
  
    // Convert to DLL 
    Node *head = NULL; 
    Node *prev = NULL;
    binarytoDll(root, &head,&prev); 
    Node *temp = head;
    cout<<"Head"<<head->data<<endl;
    while(temp!=NULL){
        cout<<temp->data<<endl;
        temp=temp->right;
    }
}