#include<bits/stdc++.h>


using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left  = NULL;
    temp->right = NULL;
}


int findinChildNode(Node* root,int key,int level){

    if(root == NULL){
        return -1;
    }

    if(root->data  ==  key){
        return level;
    }

    int leftflag = findinChildNode(root->left,key,level+1);
    if(leftflag == -1){
        int rightflag = findinChildNode(root->right,key,level+1);
        if(rightflag == - 1){
            return -1;
        }
        else
        {
            return rightflag;
        }
    }
    else{
        return leftflag;
    }
}

Node* find