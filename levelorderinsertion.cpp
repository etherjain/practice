#include<bits/stdc++.h>

struct Node{
    int data;
    Node* left=NULL;
    Node* right=NULL;
};

Node* levelOrderInsertion(Node* root,int key)
{
    if(root==NULL)
    {
        return;
    }
    Node* temp_root = root;
    std::queue<Node*> q;
    q.push(root);
    while(!q.empty())
    {
        Node *temp= q.front();
        q.pop();
        if(temp->left!=NULL)
        {
            q.push(temp->left);
        }
        else
        {
            temp->left = new Node;
            temp->left->data=key;
            break;
        }

        if(temp->right!=NULL)
        {
            q.push(temp->right);
        }
        else{
            temp->right = new Node;
            temp->right->data = key;
            break;
        }
    }
    return temp_root;
}