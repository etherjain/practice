#include<bits/stdc++.h>
using namespace std;

int binary_search(vector<int> temp,int l,int h,int value)
{
    if(l<=h)
    {
        int mid = (l+h)/2;
        if(temp[mid]==value)
        {
            return mid;
        }
        else if(temp[mid] > value)
        {
            return binary_search(temp,l,mid-1,value);
        }
        else{
            return binary_search(temp,mid+1,h,value);
        }
    }

    return -1;
}

int main(){
    int test;
    cin>>test;
    while(test--)
    {
        int number;
        cin>>number;
        vector<int> arr;
        for(int i=0;i<number;i++)
        {
            int temp;
            cin>>temp;
            arr.push_back(temp);
        }
        int value;
        cin>>value;
        cout<<"value"<<binary_search(arr,0,arr.size()-1,value)<<endl;
    }
}