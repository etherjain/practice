#include<bits/stdc++.h>

using namespace std;

struct Ranking{
    int parent;
    int rank;
};


int find(Ranking ranks[],int index)
{
    /*

    Find using path compression
    */

    if(ranks[index].parent == index)
    {
        return index;
    }
    ranks[index].parent = find(ranks,ranks[index].parent);
    return ranks[index].parent; 
}

void Union(Ranking ranks[],int index1,int index2)
{
    /*
    Union using union of rank
    */
   int temp_root1 = find(ranks,index1);
   int temp_root2 = find(ranks,index2);

   if(ranks[temp_root1].rank > ranks[temp_root2].rank)
   {
       ranks[temp_root2].parent = temp_root1; 
   }
   else if(ranks[temp_root1].rank < ranks[temp_root2].rank)
   {
       ranks[temp_root1].parent = temp_root2;
   }
   else
   {
       ranks[temp_root2].parent = temp_root1;
       ranks[temp_root1].rank++;
   }
}

vector<int> graph[10000];
int visited[10000];
int main(){
    int vertices,edges;
    cin>>vertices>>edges;
    Ranking ranks[vertices+1];
    vector< pair<int,int>> edges_list;
    for(int i=0;i<edges;i++)
    {
        int temp1,temp2;
        cin>>temp1>>temp2;
        graph[temp1].push_back(temp2);
        edges_list.push_back(make_pair(temp1,temp2));
    }

    for(int i=1;i<=vertices;i++)
    {
        ranks[i].parent = i;
        ranks[i].rank = 0;
    }

    for(int i=0;i<edges_list.size();i++)
    {
        int temp_index1 = edges_list[i].first;
        int temp_index2 = edges_list[i].second;
        
        int root1 = find(ranks,temp_index1);
        int root2 = find(ranks,temp_index2);
        if(root1 == root2)
        {
            cout<<"Cycle"<<endl;
            break;
        }
        Union(ranks,root1,root2);

    }
    cout<<"No Cycle"<<endl;
    return 0;
}