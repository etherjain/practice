#include<bits/stdc++.h>

// 5 6 7 8 1 2 3 4

int inflectionIndex(std::vector<int> arr,int l,int r)
{
    while(l<=r){
        if(l==r)
        {
            return r;
        }
        int mid=(l+r)/2;
        if(arr[mid]<arr[r])
        {
            r=mid;
        }
        else
        {
            l=mid+1;
        }
    }
    return -1;
}

int main(){
    int test;
    std::cin>>test;
    while(test--)
    {
        int number;
        std::cin>>number;
        std::vector<int> arr;
        for(int i=0;i<number;i++)
        {
            int temp;
            std::cin>>temp;
            arr.push_back(temp);

        }
        std::cout<<inflectionIndex(arr,0,arr.size()-1)<<std::endl;

    }
}