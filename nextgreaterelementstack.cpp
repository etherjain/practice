#include<bits/stdc++.h>

using namespace std;


int main(){
    int number;
    cin>>number;
    vector<int>arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }
    int result[number] = {-1};
    result[number-1] = -1;
    stack<int> st;
    st.push(arr[number-1]);
    for(int i=number-2;i>=0;i--){
        while(!st.empty() && st.top() < arr[i]){
            st.pop();
        }
        if(!st.empty()){
            result[i] = st.top();
        }
        else
        {
            result[i] = -1;
        }
        st.push(arr[i]);
    }
    for(int i=0;i<number;i++){
        cout<<result[i]<<" ";
    }
    cout<<endl;
}