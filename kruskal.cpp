#include<bits/stdc++.h>

using namespace std;

#define PII pair<long long,pair< int,int > >
const int MAX = 1e4+5;

vector<PII> graph;

int parent_arr[MAX];
void initialize(){
    for(int i=0;i<MAX;i++){
        parent_arr[i] = i;
    }
}
int root(int x){
    // if(parent_arr[x] = x){
    //     return x;
    // }
    // else{
    //     root(parent_arr[x]);
    while(parent_arr[x] != x)
    {
        parent_arr[x] = parent_arr[parent_arr[x]];
        x = parent_arr[x];
    }
    return x;
}
void uniont(int x,int y)
{
    int p = root(x);
    int q = root(y);
    parent_arr[p] = parent_arr[q];
}       
int main(){
    initialize();
    int count = 0 ;
    int vertices,edges;
    cin>>vertices>>edges;
    for(int i=0;i<edges;i++){
        int temp1,temp2,weight;
        cin>>temp1>>temp2>>weight;
        graph.push_back(make_pair(weight,make_pair(temp1,temp2)));
    }
    sort(graph.begin(),graph.end());
    for(int i=0;i< graph.size();i++){
        int node1 = graph[i].second.first;
        int node2 = graph[i].second.second;
        int weight = graph[i].first;
        // cout<<weight<<endl;
        if(root(node1)!=root(node2)){
            uniont(node1,node2);
            count+=weight;
        }
    }
    cout<<count<<endl;
    return 0;
}