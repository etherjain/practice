#include<bits/stdc++.h>
//Height is the horizontal distance 

using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
    int height;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}


void bottomtraversalmap(Node* root){
    //queue based travesal 
    map<int,int> m;
    int hd = 0;
    root->height = hd;
    queue<Node*> qt;
    qt.push(root);
    while(!qt.empty()){
        Node* temp = qt.front();
        m[temp->height] = temp->data;
        qt.pop();
        int temp_hd = temp->height;
        if(temp->left != NULL){
            temp->left->height = temp->height - 1;
            qt.push(temp->left);
        }
        if(temp->right != NULL){
            temp->right->height = temp->height + 1;
            qt.push(temp->right);
        }
    }
    map<int,int>::iterator  it;
    cout<<"Bottoms view"<<endl;
    for(it = m.begin();it!=m.end();it++)
    {
        cout<<it->second<<" ";
    }
    return;
}

void preorderhash(Node* root,int hd, int curr_height,map<int, pair<int,int>> m){

    if(root == NULL){
        return;
    }
    if(m.find(hd) == m.end()){
        m[hd] = make_pair(root->data,curr_height);
    }
    else{
        pair<int,int> p = m[hd];
        if(p.second <= curr_height)
        {
            m[hd].second = curr_height;
            m[hd].first = root->data;
        }
        
    }
    preorderhash(root->left,hd-1,curr_height + 1,m);
    preorderhash(root->right,hd+1,curr_height + 1, m);
}
int main(){
    Node *root = addNode(20); 
    root->left = addNode(8); 
    root->right = addNode(22); 
    root->left->left = addNode(5); 
    root->left->right = addNode(3); 
    root->right->left = addNode(4); 
    root->right->right = addNode(25); 
    root->left->right->left = addNode(10); 
    root->left->right->right = addNode(14); 
    bottomtraversalmap(root);
    return 0;
}