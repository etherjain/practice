#include<bits/stdc++.h>

using namespace std;

struct Node{
    int data;
    Node* left;
    Node* right;
};

Node* addNode(int data){
    Node* temp = new Node;
    temp->left = NULL;
    temp->right = NULL;
    temp->data = data;
    return temp;
}


bool checkmirror(Node* r1,Node* r2){
    if(r1 == NULL and r2 == NULL){
        return true;
    }
    else
    {
        if(r1 == NULL && r2 != NULL){
            return false;
        }
        if(r1 !=NULL && r2 == NULL){
            return false;
        }
        if(r1->data != r2->data){
            return false;
        }
        return checkmirror(r1->right ,r2->left) && checkmirror(r1->left,r2->right);
    }
}

int main(){
    Node *root        = addNode(1); 
    root->left        = addNode(2); 
    root->right       = addNode(2); 
    root->left->left  = addNode(3); 
    root->left->right = addNode(4); 
    root->right->left  = addNode(4); 
    root->right->right = addNode(3); 
    cout<<"IS MIRROR? "<<checkmirror(root,root);

}