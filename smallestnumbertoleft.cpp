#include<bits/stdc++.h>

using namespace std;

int main(){
    int number;
    vector<int> temp;
    cin>>number;
    for(int i=0;i<number;i++)
    {
        int temp2;
        cin>>temp2;
        temp.push_back(temp2);
    }

    stack<int> st;
    st.push(temp[0]);
    vector<int> result;
    result.push_back(INT_MAX);
    for(int i=1;i<temp.size();i++)
    {
        while(!st.empty() && st.top() > temp[i])
        {
            st.pop();
        }
        if(!st.empty())
        {
            result.push_back(st.top());
        }
        else{
            result.push_back(INT_MAX);
        }

        st.push(temp[i]);
    }

    for(int i:result)
    {
        auto t =  i==INT_MAX?'_':i;
        cout<<t<<endl;
    }   
return 0;
}
