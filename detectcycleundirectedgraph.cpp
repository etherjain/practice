#include<bits/stdc++.h>

using namespace std;

int graph[1000][1000];

int visited[1000] = {0};
int grayed[1000] = {0};

bool dfs_check(int start_vertex,int vertices){

    stack<pair<int,int> > st;
    st.push(make_pair(start_vertex,start_vertex));
    while(!st.empty()){
        pair<int,int> pt = st.top();
        visited[pt.first] = 1;
        st.pop();
        for(int i=1;i<vertices;i++){
            if(graph[pt.first][i] == 1){
                if((visited[i] == 1 || grayed[i] == 1) && i != pt.second){
                    return true;
                }
                else if (i!=pt.second){
                    grayed[i] =1 ;
                    st.push(make_pair(i,pt.first));
                }
            }
        }
    }
    return false;
}

void dfs_checkutl(int vertices){

    for(int i=1;i<vertices;i++){
        if(visited[i] != 1){
            if(dfs_check(i,vertices)){
                cout<<"Cycle"<<endl;
                return ;
            }
        }
    }
    cout<<"No cycle "<<endl;
    return ;
}
