#include<iostream>
using namespace std;
struct Node{
    int data;
    Node* next;
};


Node* addNode(int data)
{
    Node* temp = new Node;
    temp->data = data;
    temp->next= NULL;
}
Node* addNodes(int number)
{
    Node* head = NULL;
    Node* tempNode = NULL;
    for(int i=0;i<number;i++){

        int temp;
        cin>>temp;
        
        if(head ==NULL)
        {
            head = addNode(temp);
            tempNode = head;
        }
        else{
            
            tempNode->next = addNode(temp);
            tempNode = tempNode->next;
        }
    }
    return head;
}
void printNode(Node* head)
{
    while(head!=NULL)
    {
        cout<<head->data<<" ";
        head = head->next;
    }
    cout<<endl;
}
void deleteNode(Node** head)
{
    Node* temp = *head;
    
    Node* temp2 = temp->next;
    temp->data = temp2->data;
    temp->next = temp2->next;
    free(temp2);    
}
int main(){
    int number;
    cin>>number;
    Node* head = addNodes(number);
    deleteNode(&head);
    printNode(head);
}