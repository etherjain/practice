#include<bits/stdc++.h>


using namespace std;

#define MAX_c 26


struct TrieNode{
    TrieNode* max_char[MAX_c];
    bool eOW = false;
};


TrieNode* newNode(){
    TrieNode* temp_node = new TrieNode;
    temp_node->eOW = false;
    for(int i=0;i<MAX_c;i++){
        temp_node->max_char[i] = NULL;
    }
    return temp_node;
}

void insert(TrieNode* root,string data){
    TrieNode* temp = root;
    for(int i=0;i<data.length();i++){
        int value = int(data[i])- int('a');
        if(temp->max_char[value] == NULL){
            temp->max_char[value] = newNode();
            temp = temp->max_char[value];
            if(i == data.length()-1){
                temp->eOW = true;
            }
        }
        else{
            temp = temp->max_char[value];
            if(i == data.length()-1){
                temp->eOW = true;
            }
        }
    }
}

bool search(TrieNode* root,string data){
int eow_f = 0;
TrieNode* temp = root;
for(int i=0;i<data.length();i++){
    int value = int(data[i]) - int('a');
    if(temp->max_char[value] == NULL){
        cout<<"not found"<<endl;
        return false;
    }
    else
    {
        temp = temp->max_char[value];
        if( i == (data.length()-1) && temp->eOW ==  true){
            eow_f = 1;
        }
    }
}
if(eow_f){
    return true;
}
else{
    return false;
}
}

bool isEmpty(TrieNode* root){
    if(root == NULL){
        return true;
    }
    for(int i=0;i<MAX_c;i++){
        if(root->max_char[i] != NULL){
            return false;
        }
    }
    return true;
}

TrieNode* searchNumberUtil(TrieNode* root,string name){

}

void searchNumber(TrieNode* root,string name){
    if(root == NULL)
    return;

    if(name.length() == 0){
        return;
    }
    string prefix = "";
    TrieNode* temp_node = root;
    for(int i=0;i<name.length();i++){
        prefix+=name[i];
        int last_value = int(prefix[prefix.length()-1] ) - int('a');
        if(temp_node->max_char[last_value] == NULL){
            cout<<"not found "<<endl;
            return;
        }
        else
        {
            
        }
        
    }
    return;
}
TrieNode* remove(TrieNode* root,string data,int depth){
    if(root == NULL){
        return NULL;
    }
    if(data.length() == 0){
        return NULL;
    }

    if(depth == data.length()){
        if(root->eOW){
            root->eOW = false;
        }
        if(isEmpty(root)){
            delete(root);
            return NULL;
        }
        else
        {
            return root;
        }
        
    }

    int value = int(data[depth]) - int('a');
    root->max_char[value] = remove(root->max_char[value],data,depth+1);

    if(isEmpty(root) && root->eOW == false){
        delete(root);
        return NULL;
    }
    return root;
}

int main(){
    TrieNode* root = newNode();
    int test;
    cin>>test;
    while(test--){
        int query;
        cin>>query;
        if(query == 1){
            string data;
            cin>>data;
            insert(root,data);
        }
        else{
            string data;
            cin>>data;
            cout<<"Search Status "<<search(root,data);
        }
    }
    return 0;
}
