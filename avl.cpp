#include<bits/stdc++.h>

struct Node{
    int data;
    struct Node *left=NULL;
    struct Node *right=NULL;
    int height=1;
};

int heightNode(Node* N)
{
    if(N){
        return N->height;
    }
    else
    {
        return 0;
    }
}

Node* rightRotate(Node *y)
{
    Node* x=y->left;
    Node* temp=x->right;
    x->right=y;
    y->left=temp;

    y->height=std::max(heightNode(y->left),heightNode(y->right))+1;
    x->height=std::max(heightNode(x->left),heightNode(x->right))+1;
    return x;
}

Node* leftRotate(Node *x)
{
    Node* y=x->right;
    Node* temp=y->left;
    y->left = x;
    x->right = temp;

    x->height = std::max(heightNode(x->left),heightNode(x->right))+1;
    y->height = std::max(heightNode(y->left),heightNode(y->right))+1;
    return y; 
}

int getBalance(Node* N)
{
    if(N)
    {
        return (heightNode(N->left)-heightNode(N->right));
    }
    else
    {
        return 0;
    }
}
Node* newNode(int key)
{
    Node* temp=new Node;
    temp->data=key;
    return temp;
}
Node* getLowerNode(Node* N)
{
    Node* currentNode = N;
    if(currentNode->left!=NULL)
    {
        getLowerNode(currentNode->left);
    }
    return currentNode;
}
Node* deleteNode(Node* root,int key)
{
    if(root==NULL)
    {
        return root;
    }
    if(key < root->data)
    {
        root->left = deleteNode(root->left,key);
    }
    else if(key > root->data)
    {
        root->right = deleteNode(root->right,key);
    }
    else
    {
        if(root->left==NULL || root->right == NULL)
        {
            std::cout<<"Inside one child"<<std::endl;
            //Handling single child node
            Node* temp = root->left?root->left:root->right;
            if(temp==NULL)
            {
                std::cout<<"No child"<<std::endl;
                temp = root;
                root = NULL;
            }
            else
            {
                *root = *temp;
            }
            std::cout<<"deleing Node"<<std::endl;
            free(temp);
            std::cout<<"Deleted Node"<<std::endl;
        }
        else
        {
            std::cout<<"Got two "<<std::endl;
            //get inorder successor
            Node *temp = getLowerNode(root->right);
            root->data = temp->data;
            root->right = deleteNode(root->right,root->data);
        }

    }
    if(root==NULL)
    {
        return root;
    }
    root->height  = 1+std::max(heightNode(root->left),heightNode(root->right));

    int balance =getBalance(root);
    std::cout<<"Root Balance Deletion"<<balance<<std::endl;
    if(balance >1 && getBalance(root->left)>=0)
    {
        std::cout<<"LL"<<std::endl;
        return rightRotate(root);
    }
    
    if(balance >1 && getBalance(root->left)<0)
    {
        root->left = leftRotate(root->left);
       return rightRotate(root);
    }
    if(balance <0 && getBalance(root->right)<=0)
    {   
        return leftRotate(root);
    }
    if(balance <0 && getBalance(root->right)>0)
    {
        root->right = rightRotate(root->right);
        return leftRotate(root);
    }
    return root;
}

Node* insert(Node* root,int key)
{
    std::cout<<"In insert"<<std::endl;
    if(root==NULL) 
    {
        std::cout<<"Added new Node"<<std::endl;
        return newNode(key);
    }

    if(key > root->data)
    {
    root->right = insert(root->right,key);
    }
    else if(key< root->data)
    {
        root->left = insert(root->left,key);
    }
    else{
        return root;
    }

    root->height = 1 + std::max(heightNode(root->left),heightNode(root->right));
    std::cout<<"Getting Balance"<<std::endl;
    int balance= getBalance(root);
    std::cout<<"Root Balance "<<balance<<std::endl;
    if(balance>1 && key < root->left->data)
    {
        return rightRotate(root);
    }
    
    if(balance<-1 && key > root->right->data)
    {
        return leftRotate(root);
    }

    if(balance > 1 && key > root->left->data)
    {
        root->left = leftRotate(root->left);
        return rightRotate(root);        
    }
    if(balance <-1 && key < root->right->data)
    {
        root->right = rightRotate(root->right);
        return leftRotate(root);
    }
    return root;
}

void printInorder(Node* root)
{
    if(root==NULL)
    {
        return;
    }

    printInorder(root->left);
    std::cout<<root->data<<" "<<root->height<<" ";
    printInorder(root->right);
}
int main(){

    Node *root = NULL;
    root = insert(root, 10); 
    root = insert(root, 20); 
    root = insert(root, 30); 
    root = insert(root, 40); 
    root = insert(root, 50); 
    root = insert(root, 25);
    printInorder(root);
    root = deleteNode(root,40);
    std::cout<<std::endl;
    printInorder(root);
    std::cout<<std::endl;
    root = deleteNode(root,30);
    printInorder(root);
    return 0;
}