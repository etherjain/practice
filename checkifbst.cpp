#include<bits/stdc++.h>


using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
    int height;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

bool checkBST(Node* root,int min, int max){

    if (root == NULL){
        return true;
    }
    if(root->data >= min && root->data <= max){

        return checkBST(root->left,min,root->data-1 ) && checkBST(root->right,root->data + 1,max);
    }
    else
    {
        return false;
    }
}

int main(){
    Node* root = addNode(10);
    root->left = addNode(9);
    root->right = addNode(20);
    root->right->left = addNode(19);
    root->right->right = addNode(21);
    cout<<"IS BST "<<checkBST(root,INT_MIN,INT_MAX)<<endl;
    return 0;
}