//check row with maximum number of ones



#include<bits/stdc++.h>

using namespace std;


int binary_search(vector<int> arr,int low,int high){
    if (low == high){
        return low;
    }
    else
    {
        int mid = (low + high)/ 2;
        if(arr[mid] == 0){
            return binary_search(arr,mid + 1,high);
        }
        else{
            return binary_search(arr,low,mid);
        }
    }
}

int main(){
    int number1,number2;
    cin>>number1>>number2;
    int arr[number1][number2];
    memset(arr,0,sizeof(arr));
    for(int i=0;i<number1;i++){
        for(int j=0;j<number2;j++){
            cin>>arr[i][j];
        }
    }
    int low_index = number2;
    int max = -1;
    for(int i=0;i<number1;i++){
        
        vector<int> temp;
        for(int j=0;j<number2;j++)
        {
            temp.push_back(arr[i][j]);
        }
        int index = binary_search(temp,0,number2-1);
        if(low_index > index){
            low_index = index;
            max = i;
        }
    }

    cout<<"MAX "<<max<<endl;
    return 0;

}