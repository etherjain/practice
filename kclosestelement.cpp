#include<bits/stdc++.h>


int crossoverPoint(std::vector<int>arr,int l,int r,int key)
{
    if(arr[0]>key)
    {
        return 0;
    }
    if(arr[arr.size()-1]<key){
        return arr.size()-1;
    }
    while(r-l>1)
    {

        if(l==r)
        {
            return l;
        }
        int mid=(l+r)/2;

        if(arr[mid]<=key)
        {
            l=mid;
        }
        else
        {
            r=mid;
        }
    }
    return l;
}

std::vector<int> kNearestNumber(std::vector<int> arr,int pivot,int key,int k)
{
    int l=0;
    int r=0;
    if(arr[pivot]==key)
    {   
        l=pivot-1;
        r=pivot+1;
    }else{
        l=pivot;
        r=pivot+1;
    }
    int count=0;
    std::vector<int>answer;
    while(l>=0 && r<arr.size()&& count<k)
    {
        if(key-arr[l] <= arr[r]-key)
        {
            answer.push_back(arr[l]);
            l--;
        }
        else
        {
            answer.push_back(arr[r]);
            r++;
        }
        count++;
    }
    while(count<k && l>=0)
    {
        answer.push_back(arr[l]);
        l--;
        count++;
    }
    while(count<k && r<=arr.size()-1)
    {
        answer.push_back(arr[r]);
        r++;
        count++;
    }

    return answer;
}
int main()
{
    int test;
    std::cin>>test;
    while(test--)
    {
        int number;
        std::cin>>number;
        std::vector<int> arr;
        for(int i=0;i<number;i++)
        {
            int temp;
            std::cin>>temp;
            arr.push_back(temp);
        }
        int value;
        int k;
        std::cin>>value;
        std::cin>>k;
        int pivot = crossoverPoint(arr,0,arr.size()-1,value);
        std::cout<<"value"<<pivot<<std::endl;
        std::vector<int> result = kNearestNumber(arr,pivot,value,k);
        for(int i=0;i<result.size();i++)
        {
            std::cout<<result[i]<<" ";
        }
        std::cout<<std::endl;
    }
    return 0;
}