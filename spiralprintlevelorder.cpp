#include<bits/stdc++.h>
//Print tree in level order in spiral form

using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
    int height;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->left = NULL;
    temp->right = NULL;
    return temp;
}

void spiralLevelOrder(Node* root){
    if(root == NULL){
        return;
    }
    int flag = 0;
    queue<Node*> st1;
    stack<Node*> st2;
    st1.push(root);
    while(!st1.empty()|| !st2.empty()){
        while(!st1.empty()){
            Node* temp = st1.front();
            cout<<temp->data<<" ";
            st1.pop();
            if(temp->right){
                st2.push(temp->right);
            }
            if(temp->left){
                st2.push(temp->left);
            }
        }
        cout<<endl;
        while(!st2.empty()){
            Node*temp = st2.top();
            cout<<temp->data<<" ";
            st2.pop();
            if(temp->left){
                st1.push(temp->left);
            }
            if(temp->right){
                st1.push(temp->right);
            }
        }
        cout<<endl;
    } 
}
//TODO 

int main(){
    Node* root = addNode(11);
    root->left = addNode(9);
    root->right = addNode(12);
    root->left->left = addNode(7);
    root->left->right = addNode(10);
    spiralLevelOrder(root);
    return 0;
}