#include<bits/stdc++.h>

using namespace std;

int main(){
    string s;
    cin>>s;
    int visited[26];
    memset(visited,-1,sizeof(visited));
    int start  = 0;
    int rear =  0;
    int count = 0;
    while(rear!=s.length())
    {
        cout<<"Start"<<" "<<start<<endl;
        cout<<"Rear"<<rear<<endl;
        int temp = int(s[rear])-int('a');
        cout<<"Temp "<<temp<<endl;
        if(visited[temp] == -1){
            visited[temp] = rear;
            count++;
        }
        else if(visited[temp]>=start)
        {
            start = visited[temp]+1;
            visited[temp] = rear;
            count++;
        }
        else
        {
            visited[temp] = rear;
            count++;
        }
        rear++;
    }
    cout<<"count "<<count<<endl;
    return 0;
}