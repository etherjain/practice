#include<bits/stdc++.h>

using namespace std;

#define MAX 10000

void update(int bit[],int index,int number,int size)
{
    // cout<<"update"<<endl;
    for(;index<=size;index+=index&-index)
    {
        // cout<<index<<endl;
        bit[index]+=number;
    }
}

int query(int bit[],int index)
{
    int sum=0;
    for(;index>0;index-=index&-index){
        sum+=bit[index];
    }
    return sum;
}
int main(){
    int number;
    cin>>number;
    int bit[MAX];
    memset(bit,0,sizeof(bit));
    for(int i=1;i<=number;i++){
        int temp;
        cin>>temp;
        update(bit,i,temp,number);
    }
    for(int i=1;i<=number;i++){
        cout<<bit[i]<<" ";
    }
    cout<<endl;
    cout<<query(bit,2)<<endl;
    return 0;
}