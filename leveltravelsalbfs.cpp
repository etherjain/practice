#include<bits/stdc++.h>

using namespace std;

vector<int> graph[10000];
int visited[10000] = {0};

void bfs(int root)
{
    queue<int> q;
    q.push(root);
    visited[root]=1;
    int count = 1;
    while(!q.empty())
    {
        int top_number = q.front();
        cout<<"Top Number"<<top_number<<endl;
        q.pop();
        for(int i=0;i<graph[top_number].size();i++)
        {
            int temp = graph[top_number][i];
            if(!visited[temp])
            {
                cout<<"temp "<<temp<<endl;
                q.push(temp);
                visited[temp] = visited[top_number]+1;        
            }
        }
    }
}

int main()
{
    int vertices,edges;
    cin>>vertices>>edges;
    for(int i=0;i<edges;i++)
    {
        int number1,number2;
        cin>>number1>>number2;
        graph[number1].push_back(number2);
        // graph[number2].push_back(number1);
    }

    for(int i=0;i<vertices;i++)
    {
        if(visited[i] == 0)
        {
            bfs(i);
        }
    }

    for(int i=0;i<vertices;i++)
    {
        cout<<i<<" "<<visited[i]<<endl;
    }
}

