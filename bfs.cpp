#include<bits/stdc++.h>

using namespace std;

vector<int> graph[10000];
int visited[10000]={0};
void bfs_util(int);
void bfs_util(int number)
{
    if(visited[number])
    {
        return;
    }
    queue<int> q;
    q.push(number);
    visited[number]=1;
    while(!q.empty())
    {
        int top = q.front();
        cout<<"top"<<top<<" ";
        q.pop();
        for(int i=0;i<graph[top].size();i++)
        {
            if(!visited[graph[top][i]])
            {
                q.push(graph[top][i]);
                visited[graph[top][i]]=1;
            }
        }
    }
}
void bfs(int vertices)
{
    for(int i=0;i<vertices;i++)
    {
        cout<<"Util Called"<<endl;
        if(!visited[i])
        {
            bfs_util(i);
        }
    }
}
int main()
{
    int number;
    cin>>number;
    while(number--)
    {
        int vertices,edges;
        cin>>vertices>>edges;
        for(int i=0;i<edges;i++)
        {
            int temp1,temp2;
            cin>>temp1>>temp2;
            graph[temp1].push_back(temp2);
        }
        bfs(vertices);
    }
    
    return 0;
}