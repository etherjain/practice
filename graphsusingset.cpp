#include<bits/stdc++.h>

using namespace std;

/*
Graph Using set and unordered set
*/

set<int, greater<int> > graph[10000];


unordered_set<int> graph2[10000];

int visited[10000]=  {0};

int main(){
    int test;
    cin>>test;
    while(test--)
    {
        int vertices,edges;
        cin>>vertices>>edges;
        for(int i=0;i<edges;i++)
        {
            int temp1,temp2;
            cin>>temp1>>temp2;
            graph[temp1].insert(temp2);
            graph[temp2].insert(temp1);
            graph2[temp1].insert(temp2);
            graph2[temp2].insert(temp1);
        }
        int sudotest;
        cin>>sudotest;
        while(sudotest--)
        {
            int temp1,temp2;
            cin>>temp1>>temp2;
            if(graph[temp1].find(temp2) != graph[temp1].end())
            {
                cout<<"found in set"<<endl;
            }
            else
            {
                cout<<" Not found"<<endl;
            }
            if(graph2[temp1].find(temp2) != graph2[temp1].end())
            {
                cout<<"found in unordered set"<<endl;
            }
            else{
                cout<<"Not found in ordered set"<<endl;
            }
        }

    }
}