#include<bits/stdc++.h>

using namespace std;


struct Node{
    int data;
    Node* left;
    Node* right;
};


Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->right = NULL;
    temp->left = NULL;
    return temp;
}


int diameter(Node* root,int &maxdiameter){
    if (root == NULL){
        return 0;
    }
    int ld = diameter(root->left,maxdiameter);
    int rd = diameter(root->right,maxdiameter);
    if(root->left !=NULL || root->right!=NULL){
        maxdiameter = max(maxdiameter,ld+rd+1);
        return max(ld,rd) + 1;
    }
}

int main(){
    Node *root = addNode(-15); 
    root->left = addNode(5); 
    root->right = addNode(6); 
    root->left->left = addNode(-8); 
    root->left->right = addNode(1); 
    root->left->left->left = addNode(2); 
    root->left->left->right = addNode(6); 
    root->right->left = addNode(3); 
    root->right->right = addNode(9); 
    root->right->right->right= addNode(0); 
    root->right->right->right->left= addNode(4); 
    root->right->right->right->right= addNode(-1); 
    root->right->right->right->right->left= addNode(10);
    int maxdiameter = INT_MIN;
    diameter(root,maxdiameter);
    cout<<"MAX Diameter "<<maxdiameter<<endl;
    return 0;

}