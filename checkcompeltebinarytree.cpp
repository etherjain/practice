#include<bits/stdc++.h>

using namespace std;

struct Node{
    int data;
    Node* right = NULL;
    Node* left = NULL;
};

int countnodes(Node* root){
    if(root == NULL){
        return 0;
    }
    return 1+countnodes(root->right)+countnodes(root->left);
}

bool completeTree(Node* root,int index,int countNodes){
    if(root == NULL){
        return true;
    }
    if(index >= countNodes){
        return false;
    }
    return (completeTree(root->left,2*index+1,countNodes) && completeTree(root->right,2*index+2,countNodes));
}

bool completeTreeIterative(Node* root){

    queue<Node*> qt;
    if(root){
        qt.push(root);
    }
    else{
        return false;
    }
    bool flag = false;
    while(!qt.empty()){
        Node* top = qt.front();
        qt.pop();
        if(top->left){
            if(flag){
                return false;
            }
            qt.push(top->left);
        }
        else{
            flag = true;
        }
        
        if(top->right){
            if(flag){
                return false;
            }
            qt.push(top->right);
        }
        else{
            flag = true;
        }
    }
    return true;
}

Node* addNode(int data){
    Node* temp = new Node;
    temp->data = data;
    temp->right = NULL;
    temp->left = NULL;
    return temp;
}
int main(){
    Node* root = addNode(10);
    root->left = addNode(8);
    root->right = addNode(12);
    root->left->left = addNode(6);
    root->left->right = addNode(9);
    root->right->left = addNode(11);
    cout<<"iterative "<<completeTreeIterative(root)<<endl;
    int index =0 ;
    int countNode = countnodes(root);
    cout<<"node count "<<countNode<<endl;
    cout<<"recursive "<<completeTree(root,index,countNode)<<endl;

}