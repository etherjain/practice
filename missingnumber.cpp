#include<bits/stdc++.h>

using namespace std;

int missingNumber(vector<int>number,int l,int r)
{
    int mid;
    while(r-l>1)
    {
        mid=(r+l)/2;
        if(number[mid]-number[l]>mid-l)
        {
            cout<<"Mid lower"<<mid<<endl;
            r=mid;
        }
        else if(number[r]-number[mid]>r-mid)
        {
            cout<<"Mid highter"<<mid<<endl;
            l=mid;
        }
        else{
            return -1;
        }
    }

    return  (number[r]+number[l])/2;
}   
int main()
{
    int test;
    cin>>test;
    while(test--)
    {
        int number;
        cin>>number;
        vector<int> arr;
        for(int i=0;i<number;i++)
        {
            int temp;
            cin>>temp;
            arr.push_back(temp);
        }

        cout<<"Missing Number "<<missingNumber(arr,0,arr.size()-1)<<endl;
    }
    return 0;
}