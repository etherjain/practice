#include<bits/stdc++.h>

using namespace std;


int main(){
    int number;
    cin>>number;
    int max_weight;
    cin>>max_weight;
    vector<int> values;
    vector<int> weights;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        values.push_back(temp);
    }
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        weights.push_back(temp);
    }

    int dp[max_weight+1];
    memset(dp,0,sizeof(dp));

    for(int i=0;i<=max_weight;i++){
        for(int j=0;j<number;j++)
        {
            if(weights[j] <= i)
            dp[i] = max(dp[i],dp[i-weights[j]]+values[j]);
        }
    }
    cout<<dp[max_weight]<<endl;;
}   