#include<bits/stdc++.h>

//find lenght of largest subarray with sum 0;

using namespace std;

int main(){
    int number;
    cin>>number;
    vector<int> arr;
    for(int i=0;i<number;i++){
        int temp;
        cin>>temp;
        arr.push_back(temp);
    }
    unordered_map<int,int> mt;

    int sum = 0;
    int max_len = 0;
    for(int i=0;i<number;i++){
        sum += arr[i];
        
        if(arr[i] == 0 && max_len == 0){
            max_len = 1;
        }
        if(sum == 0){
            max_len = i + 1;
        }

        if(mt.find(sum) != mt.end()){
            max_len = max(max_len,i-mt[sum]);
        }
        else{
            mt[sum] = i;
        }
        
    }
    cout<<"Maxlen "<<max_len<<endl;
    return 0;

}